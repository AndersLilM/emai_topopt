function [mesh,opt] = ElectricMeshGenerator_Q9(Lx,Ly,nelx,nely,E,nu,rho,thk,support,opt)
% Simple, structured mesh generator
%
% mesh = StructMeshGenerator(Lx,Ly,nelx,nely,E,nu,rho,thk,support)
%
% INPUTS: Lx, Ly:       dimensions nelx, nely:   number of elements in the
% grid E,nu,rho,thk: Material and thickness parameters support:
% 'simple' or 'clamped'
%
% OUTPUT: "mesh" structure with fields: X:            nodal coordinates IX:
% topology table Material:     [thk E nu dens] bound:        list of BCs
% PointLoads:    a single in the center PressureLoads: distributed pressure
% load

% Derived data
mesh.nelx =nelx;
mesh.nely = nely;
nel = nelx*nely;
nny = 1+nely*2;
mesh.nny = nny;
nnx = 1+nelx*2;
mesh.nnx = nnx;
nn =  nny*nnx;
mesh.neqn = 2*nn;

% Containers
mesh.X = zeros(nn,3);
mesh.IX = zeros(nel,6+5);
mesh.Material = [thk E nu rho];
mesh.bound = []; % clamped or simple
mesh.PointLoads = []; % in the center (or as close as poss.)
mesh.SurfLoads = []; % evenly distributed
% Name the mesh - with elements
mesh.filename = ['RectMeshQ9_' num2str(nelx) 'x' num2str(nely) ...
    '_E_' num2str(E) '_nu_' num2str(nu) '_rho_' num2str(rho) ...
    '_thk_' num2str(thk) '_' support ];
% Nodes
n=0;
dx=Lx/nelx/2;
dy=Ly/nely/2;
for nx=1:2*nelx+1
    for ny=1:2*nely+1
        n=n+1;
        mesh.X(n,1) = n;
        mesh.X(n,2) = (nx-1)*dx;
        mesh.X(n,3) = (ny-1)*dy;
    end
end

% Elements
e=0;
eActive = 0;
ePassive = 0;
eBase = 0;
eTop = 0;
eBot = 0;
eLoad = [];
ite = 0;
itel = 0;
itepL = 0;
itepR = 0;
itea = 0;
Eflag = 1;
eDom = 0;
for elx=1:nelx
    for ely=1:nely
        e=e+1;
        n1 = 2*nny*(elx-1) + 2*ely-1;
        n2 = 2*nny*(elx)   + 2*ely-1;
        n3 = 2*nny*(elx)   + 2*ely+1;
        n4 = 2*nny*(elx-1) + 2*ely+1;
        n5 = nny*(2*elx-1) + 2*ely-1;
        n6 = 2*nny*(elx)   + 2*ely;
        n7 = nny*(2*elx-1) + 2*ely+1;
        n8 = 2*nny*(elx-1) + 2*ely;
        n9 = nny*(2*elx-1) + 2*ely;
        edof = [n1 n2 n3 n4 n5 n6 n7 n8 n9];
        mesh.IX(e,1)=e;
        mesh.IX(e,2:10)=edof;
        
        %if elx> 1/4*nelx &&  elx <= 3/4*nelx && ely>1.25/3*nely &&  ely<=1.75/3*nely
            %color = color+1;
            %eActive = eActive +1;
            
            %opt.topopt.indActive(eActive) = e;
%             if rem(color,2)>0
             opt.topopt.x(e,1) = 1;%opt.topopt.volfrac;
%             else
%                 opt.topopt.x(e,1) = 1;
%             end
        %else
            opt.topopt.x(e,1) = 0;
        %end
        
        
        
        if ely<= (100-36)/100*nely && ely>= (100-37)/100*nely && elx>=37/100*nelx && elx<=64/100*nelx %ely> 5/15*nely && ely<= 9/15*nely && elx<=10/13*nelx
            %color = color+1;
            %ePassive = ePassive +1;
            %opt.topopt.indPassive(ePassive) = e;
            %             if rem(color,2)>0
            %opt.topopt.x(e,1) = 1;
            
            %evec(ePassive) = e;
            %if elx == 1
            %    ite = ite +1;
            %    evec(ite) = e;
            %end
        elseif ely>= (37)/100*nely && ely<= (38)/100*nely && elx>=37/100*nelx && elx<=64/100*nelx %ely> 5/15*nely && ely<= 9/15*nely && elx<=10/13*nelx
            %color = color+1;
            %ePassive = ePassive +1;
            %opt.topopt.indPassive(ePassive) = e;
            %             if rem(color,2)>0
            %opt.topopt.x(e,1) = 1;
            
            %evec(ePassive) = e;
            %             else
            %                 opt.topopt.x(e,1) = 1;
            %             end
        elseif (ely<= 60/100*nely && ely>= 52/100*nely) || (ely<= 49/100*nely && ely>= 41/100*nely)
            eActive = eActive +1;
            
            opt.topopt.indActive(eActive) = e;
            opt.topopt.x(e,1) = 1;
            if elx == 50
                eDisp = e;
            end
            
            if elx == 1
                itepL = itepL +1;
                ePotLeft(itepL) = e;
            end
            
            if elx == nelx
                itepR = itepR +1;
                ePotRight(itepR) = e;
            end
            
        %elseif ely> 9/30*nely && ely<= 13/30*nely && elx<=10/13*nelx
        %    eActive = eActive +1;
            
         %   opt.topopt.indActive(eActive) = e;
        %    opt.topopt.x(e,1) = 1;
        elseif (ely>= 50/100*nely && ely<= 51/100*nely)
            
        %opt.topopt.x(e,1) = 1;
        if elx == 1
                itepL = itepL +1;
                ePotLeft(itepL) = e;
            end
            
            if elx == nelx
                itepR = itepR +1;
                ePotRight(itepR) = e;
            end
        else
            opt.topopt.x(e,1) = 0;
        end
        
%         if    elx >=11/100*nelx && elx<=90/100*nelx && ely >= 6/100*nely && ely <= 30/100*nely  %elx<=10/13*nelx && ely<= 9/30*nely %
%             eDom = eDom +1;
%             opt.objEel(eDom) = e;
%         end
        
        if    elx >=45/100*nelx && elx<=56/100*nelx && ely >= 3/100*nely && ely <= 15/100*nely  %elx<=10/13*nelx && ely<= 9/30*nely %
            eDom = eDom +1;
            opt.objEel(eDom) = e;
        end
        
        
        % fixed material ! only ONE !
        mesh.IX(e,11)=1;
    end
end

%opt.Base = Base;
%evecleft = 1;
%evecright = 0;
%% SET UP BOUNDARY CONDITIONS
leftdofs = 1:nny;
rightdofs = (1:nny)+nny*(nnx-1);
for i = 1:nnx
    %lowerdofs(i*3-2:i*3) = [1+nny*3*(i-1),2+nny*3*(i-1),3+nny*3*(i-1)];
    %upperdofs(i*3-2:i*3) =
    %[1+nny*3*(i-1)+(nely*q)*3,2+nny*3*(i-1)+(nely*q)*3,3+nny*3*(i-1)+(nely*q)*3];
    lowerdofs(i) = nny*(i-1)+1;
    upperdofs(i) = nny*i;
end
%fixeddofs = [leftdofs]; nfdofs = length(fixeddofs);
if strcmp(support,'capacitor')
    leftdofsDown = leftdofs(1:ceil(numel(leftdofs)/2)-1);
    leftdofsUp = leftdofs(ceil(numel(leftdofs)/2):end);
    rightdofsDown = rightdofs(1:ceil(numel(leftdofs)/2)-1);
    rightdofsUp = rightdofs(ceil(numel(leftdofs)/2):end);
    
    %mesh.bound(:,1) = fixeddofs; mesh.bound(1:nfdofs,2) = 1;
    %mesh.bound(nfdofs+1:2*nfdofs,2) = 2; mesh.bound(:,3) = 0;
    
    mesh.bound = [upperdofs',2*ones(length(upperdofs),1),zeros(length(upperdofs),1);
        lowerdofs',2*ones(length(lowerdofs),1),zeros(length(lowerdofs),1);
        leftdofs',ones(length(leftdofs),1),zeros(length(leftdofs),1);
        rightdofs',ones(length(rightdofs),1),zeros(length(rightdofs),1)];
        
%          mesh.bound = [upperdofs',2*ones(length(upperdofs),1),zeros(length(upperdofs),1);
%         lowerdofs',2*ones(length(lowerdofs),1),zeros(length(lowerdofs),1);
%         leftdofsDown',ones(length(leftdofsDown),1),zeros(length(leftdofsDown),1);
%         rightdofsDown',ones(length(rightdofsDown),1),zeros(length(rightdofsDown),1)];
    %keyboard
    %  mesh.bound =
    %  [leftdofs',ones(length(leftdofs),1),zeros(length(leftdofs),1);
    %  leftdofs',2*ones(length(leftdofs),1),zeros(length(leftdofs),1);]
    
elseif strcmp(support,'New')
     %StatElecDOF = mesh.IX([evec],2:10);
    %StatElecDOF = mesh.IX([Top'],2:10);
    %StatElecDOF = unique(reshape(StatElecDOF,1,numel(StatElecDOF)));
    
    dofvecleft = mesh.IX(ePotLeft,[1,4,8]+1);
    dofvecleft = reshape(dofvecleft,numel(dofvecleft),1);
    dofvecleft = unique(dofvecleft)';
    
    dofvecright = mesh.IX(ePotRight,[2,3,6]+1);
    dofvecright = reshape(dofvecright,numel(dofvecright),1);
    dofvecright = unique(dofvecright)';
%     mesh.bound = [upperdofs',ones(length(upperdofs),1),zeros(length(upperdofs),1);
%         upperdofs',2*ones(length(upperdofs),1),zeros(length(upperdofs),1);
%         leftdofs',ones(length(leftdofs),1),zeros(length(leftdofs),1);
%         leftdofs',2*ones(length(leftdofs),1),zeros(length(leftdofs),1);
%         rightdofs',ones(length(rightdofs),1),zeros(length(rightdofs),1);
%          rightdofs',2*ones(length(rightdofs),1),zeros(length(rightdofs),1);
%          StatElecDOF',ones(length(StatElecDOF),1),zeros(length(StatElecDOF),1);
%          StatElecDOF',2*ones(length(StatElecDOF),1),zeros(length(StatElecDOF),1);
%          lowerdofs',ones(length(lowerdofs),1),zeros(length(lowerdofs),1);
%          lowerdofs',2*ones(length(lowerdofs),1),zeros(length(lowerdofs),1);];
     
     mesh.bound = [leftdofs',ones(length(leftdofs),1),zeros(length(leftdofs),1);
        dofvecleft',2*ones(length(dofvecleft),1),zeros(length(dofvecleft),1);
        rightdofs',ones(length(rightdofs),1),zeros(length(rightdofs),1);
         dofvecright',2*ones(length(dofvecright),1),zeros(length(dofvecright),1)];
%mesh.bound = [leftdofs',ones(length(leftdofs),1),zeros(length(leftdofs),1);
 %       rightdofs',ones(length(rightdofs),1),zeros(length(rightdofs),1);]
% mesh.bound = [upperdofs',ones(length(upperdofs),1),zeros(length(upperdofs),1);
%         upperdofs',2*ones(length(upperdofs),1),zeros(length(upperdofs),1);
%         leftdofs',ones(length(leftdofs),1),zeros(length(leftdofs),1);
%         leftdofs',2*ones(length(leftdofs),1),zeros(length(leftdofs),1);
%         rightdofs',ones(length(rightdofs),1),zeros(length(rightdofs),1);
%          rightdofs',2*ones(length(rightdofs),1),zeros(length(rightdofs),1);
%          lowerdofs',2*ones(length(lowerdofs),1),zeros(length(lowerdofs),1);
%          lowerdofs',ones(length(lowerdofs),1),zeros(length(lowerdofs),1);];
     
      %mesh.bound = [upperdofs',2*ones(length(upperdofs),1),zeros(length(upperdofs),1);
      %  lowerdofs',2*ones(length(lowerdofs),1),zeros(length(lowerdofs),1);
      %  leftdofs',ones(length(leftdofs),1),zeros(length(leftdofs),1);
      %  leftdofs',2*ones(length(leftdofs),1),zeros(length(leftdofs),1);
      %  rightdofs',ones(length(rightdofs),1),zeros(length(rightdofs),1)];
    
    
elseif strcmp(support,'GIL')
    %mesh.bound = [];
    
    %LoadDofs = mesh.IX(eLoad',[1 2 5]+1);
    %LoadDofs = LoadDofs(:);
    
    StatElecDOF = mesh.IX([Top'],2:10);
    %StatElecDOF = mesh.IX([Top'],2:10);
    StatElecDOF = unique(reshape(StatElecDOF,1,numel(StatElecDOF)));
    
    
    
    %for ii = 1:length(LoadDofs)
    %    lowerdofs(lowerdofs == LoadDofs(ii)) = [];
    %end
    
    
    % fixed ends of base
    if (Ly/(nely))/0.5e-6==1
        Left_end = Base(1:2);
        Right_end = Base(end-1:end);
        nn = 6;
    else
       Left_end = Base(1:4);
        Right_end = Base(end-3:end);
        nn = 12;
    end
    %opt.topopt.x([Left_end,Right_end,eLoad]) = 0.5;
    
    LBaseDofs = reshape(mesh.IX(Left_end,[1 4 8]+1),1,nn);
    RBaseDofs = reshape(mesh.IX(Right_end,[2 3 6]+1),1,nn);
    
    % Hardwall + fixed ends of base
    mesh.bound = [leftdofs',ones(length(leftdofs),1),zeros(length(leftdofs),1);
        rightdofs',ones(length(rightdofs),1),zeros(length(rightdofs),1);
        lowerdofs',2*ones(length(lowerdofs),1),zeros(length(lowerdofs),1);
        upperdofs',2*ones(length(upperdofs),1),zeros(length(upperdofs),1);
        LBaseDofs',ones(size(LBaseDofs))',zeros(size(LBaseDofs))';
        RBaseDofs',ones(size(RBaseDofs))',zeros(size(RBaseDofs))';
        LBaseDofs',2*ones(size(LBaseDofs))',zeros(size(LBaseDofs))';
        RBaseDofs',2*ones(size(RBaseDofs))',zeros(size(RBaseDofs))';
        StatElecDOF',ones(size(StatElecDOF))',zeros(size(StatElecDOF))';
        StatElecDOF',2*ones(size(StatElecDOF))',zeros(size(StatElecDOF))'];
    
    
end



% Pointload in the center node
%mesh.PointLoads = [1 2 -1000]; keyboard

%mesh.PointLoads = [mesh.IX(eLoad(2),3) 2 1000]; p = 1e4; keyboard
%opt.e_obj = emid';
%objE = opt.topopt.indPassive(end);
edofs = 1;%mesh.IX(eDisp,[3]);
%edofs=unique(reshape(edofs,numel(edofs),1));
opt.objdispDOFs =2*edofs;

%p = 1000;
%mesh.SurfLoads = [eLoad' ones(length(eLoad),1) p*ones(length(eLoad),1)];
%mesh.SurfLoads = [Base' 2*ones(length(Base),1) p*ones(length(Base),1)];
mesh.SurfLoads = [];
mesh.wdof =  [nn 2 1];

%% Support excitation [node, amplitude]
%structEl = opt.topopt.x == 1;
%structDofsQ9 = mesh.IX(structEl,2:10);
structDofsQ9 = mesh.IX(1:end,2:10);
structDofsQ9 = unique(reshape(structDofsQ9,numel(structDofsQ9),1));
%opt.StructDofs = structDofsQ9;

%ampl = 1e4;
mesh.SuppLoadX = [];
%mesh.SuppLoadY = [structDofsQ9,ampl*ones(length(structDofsQ9),1)];
mesh.SuppLoadY = [];
[mesh,opt]=MixedMesh_Q9toQ4(mesh,opt,support,eLoad,ePotLeft,ePotRight);

end

function [mesh,opt] = MixedMesh_Q9toQ4(MESH,opt,support,eLoad,epotLeft,epotRight)

mesh.Q9 = MESH;
IX = mesh.Q9.IX;
X = mesh.Q9.X;
RemoveIndex=[];
mesh.Q4nodeIndex = [];
mesh.Q4.acDom = opt.objEel;

%% obtaining indeces for nodes 5 to 9
for i = 1:length(IX(:,1))
    RemoveIndex((end+1):end+5) =  IX(i,6:10);
    mesh.Q4nodeIndex((end+1):end+4) = IX(i,2:5);
end
% removing dublicates
RemoveIndex = unique(RemoveIndex);
mesh.Q4nodeIndex = unique(mesh.Q4nodeIndex);
mesh.Q4.Q4nodeIndex = unique(mesh.Q4nodeIndex);
%mesh.QnodeIndex = RemoveIndex;
%% Creating X matrix with new numbering
X_temp = X;
X_temp(RemoveIndex,:) = [];

mesh.Q4.X(:,1) = 1:length(X_temp(:,1));
mesh.Q4.X(:,2:3) = X_temp(:,2:3);
nnQ4 = mesh.Q4.X(end,1);

%% Substituting node numbering in IX with new numbering
IX_temp = IX(:,1:5);
mesh.Q4.IX(:,1) = IX_temp(:,1);

for i = 1:nnQ4
    IX_temp(IX_temp==X_temp(i,1)) = mesh.Q4.X(i,1);
end

mesh.Q4.IX(:,2:5) = IX_temp(:,2:5);
mesh.Q4.IX(:,6) = 1;



nn = length(mesh.Q4.X(:,1));
mesh.Q4.neqn = nn;

%% SET UP BOUNDARY CONDITIONS
elUpper = mesh.Q9.nely.*[1:mesh.Q9.nelx];
elLower = mesh.Q9.nely.*([1:mesh.Q9.nelx]-1)+1;

nnx = mesh.Q9.nelx+1;
nny = mesh.Q9.nely+1;
leftdofs = 1:nny;
rightdofs = (1:nny)+nny*(nnx-1);
lowerdofs = nny.*([1:nnx]-1)+1;
upperdofs = nny.*[1:nnx];
%for i = 1:nnx
%lowerdofs(i*3-2:i*3) = [1+nny*3*(i-1),2+nny*3*(i-1),3+nny*3*(i-1)];
%upperdofs(i*3-2:i*3) =
%[1+nny*3*(i-1)+(nely*q)*3,2+nny*3*(i-1)+(nely*q)*3,3+nny*3*(i-1)+(nely*q)*3];
%    lowerdofs(i) = nny*(i-1)+1;
%   upperdofs(i) = nny*i;
%end mesh.bound =
%[leftdofs',ones(length(leftdofs),1),zeros(length(leftdofs),1);
if strcmp(support,'capacitor')
mesh.Q4.bound = [upperdofs',ones(length(upperdofs),1),opt.phi(1)*ones(length(upperdofs),1);
    lowerdofs',ones(length(lowerdofs),1),0*ones(length(lowerdofs),1)];
%mesh.Q4.PointLoads = [floor(nn/2),1,100000];

elseif strcmp(support,'New')
    %dofvec = mesh.Q4.IX(evec,[2,5]);
    %dofvec = reshape(dofvec,numel(dofvec),1);
    %dofvec = unique(dofvec)';
    
    dofvecleft = mesh.Q4.IX(epotLeft,[2,5]);
    dofvecleft = reshape(dofvecleft,numel(dofvecleft),1);
    dofvecleft = unique(dofvecleft)';
    
    dofvecright = mesh.Q4.IX(epotRight,[3,4]);
    dofvecright = reshape(dofvecright,numel(dofvecright),1);
    dofvecright = unique(dofvecright)';
    
    
    %keyboard
    %leftdofs = [median(leftdofs)-2 median(leftdofs)-1 median(leftdofs) median(leftdofs)+1 median(leftdofs)+2];
    mesh.Q4.bound = [dofvecleft(1)',ones(length(dofvecleft(1)),1),0*ones(length(dofvecleft(1)),1);
        dofvecleft(end)',ones(length(dofvecleft(end)),1),1*ones(length(dofvecleft(end)),1);
        dofvecright(1)',ones(length(dofvecright(1)),1),0*ones(length(dofvecright(1)),1);
        dofvecright(end)',ones(length(dofvecright(end)),1),1*ones(length(dofvecright(end)),1);];
    %rightdofs = rightdofs(end);
    p = 1e3;
    %mesh.Q4pres.bound = [rightdofs',ones(length(rightdofs),1),p*ones(length(rightdofs),1)];
    %mesh.Q4pres.bound = [upperdofs',ones(length(upperdofs),1),p*ones(length(upperdofs),1);
     %   lowerdofs',ones(length(lowerdofs),1),-p*ones(length(lowerdofs),1)];
    %mesh.Q4pres.bound = [upperdofs',ones(length(upperdofs),1),p*ones(length(upperdofs),1)];
    mesh.Q4pres.bound = [];
    %pin = 0;
    mesh.Q4.absorbBC = [elLower',ones(length(elLower),1),0*ones(length(elLower),1);
        elUpper',3*ones(length(elUpper),1),p*ones(length(elUpper),1)];
    %mesh.Q4.absorbBC = [0];
    %mesh.Q4.absorbBC = [elUpper',3*ones(length(elUpper),1),p*ones(length(elUpper),1)];
        
%opt.absorbDofs = mesh.Q9.IX(elLower,[1,2,5]+1);
%opt.absorbDofs = unique(reshape(opt.absorbDofs,numel(opt.absorbDofs),1));
    %opt.DirichletPressDofs = rightdofs';
    %opt.DirichletPressDofs = upperdofs';
    %mesh.Q4pres.bound = [];
end
%mesh.objectDOFS = upperdofs;
mesh.Q4.PointLoads = [];
mesh.Q4pres.PointLoads = [];
%% SurfLoads = [e-number face pressure]
% Element faces:
%   3
% 4|_|2
%   1

%p = 1; mesh.Q4.SurfLoads =
%[(1:mesh.Q9.nely)',4*ones(mesh.Q9.nely,1),p*ones(mesh.Q9.nely,1)];
mesh.Q4.SurfLoads = [];
mesh.Q4.Material = mesh.Q9.Material;


%opt.e_obj = mesh.Q4.acDom';mesh.
mesh.Q4.ActiveEdofs = mesh.Q4.IX(opt.topopt.indActive,[1 2 3 4]+1);
mesh.Q4.ActiveEdofs = unique(reshape(mesh.Q4.ActiveEdofs,numel(mesh.Q4.ActiveEdofs),1));
mesh.Q9.ActiveEdofs = mesh.Q9.IX(opt.topopt.indActive,[2:10]);
mesh.Q9.ActiveEdofs = unique(reshape(mesh.Q9.ActiveEdofs,numel(mesh.Q9.ActiveEdofs),1));

%opt.objConDOFs = edofs;
opt.objDOFs = opt.objdispDOFs;
opt.EDOFs = mesh.Q4.IX(opt.objEel,[2:5]);
opt.EDOFs = unique(reshape(opt.EDOFs,numel(opt.EDOFs),1));
% Finding boundary dofs for pressure dofs
%nnx = mesh.Q9.nelx+1; nny = mesh.Q9.nely+1; for i = 1:nnx
%    mesh.Q4.lowerdofs(i) = nny*(i-1)+1;
%   mesh.Q4.upperdofs(i) = nny*i;
%end
%strucPass = find(opt.topopt.x(opt.topopt.indPassive)==1);
%nodes = mesh.Q9.X(mesh.Q9.IX(strucPass,3));
%[xmax index]=min(mesh.Q9.X(nodes,3));
%mesh.Q9.objDOF = nodes(index);

end



