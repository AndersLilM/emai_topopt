function [mesh,opt] = ElectricMeshGenerator_Q9(Lx,Ly,nelx,nely,E,nu,rho,thk,support,opt)
% Simple, structured mesh generator
%
% mesh = StructMeshGenerator(Lx,Ly,nelx,nely,E,nu,rho,thk,support)
%
% INPUTS: Lx, Ly:       dimensions nelx, nely:   number of elements in the
% grid E,nu,rho,thk: Material and thickness parameters support:
% 'simple' or 'clamped'
%
% OUTPUT: "mesh" structure with fields: X:            nodal coordinates IX:
% topology table Material:     [thk E nu dens] bound:        list of BCs
% PointLoads:    a single in the center PressureLoads: distributed pressure
% load

% Derived data
mesh.nelx =nelx;
mesh.nely = nely;
nel = nelx*nely;
nny = 1+nely*2;
mesh.nny = nny;
nnx = 1+nelx*2;
mesh.nnx = nnx;
nn =  nny*nnx;
mesh.neqn = 2*nn;

% Containers
mesh.X = zeros(nn,3);
mesh.IX = zeros(nel,6+5);
mesh.Material = [thk E nu rho];
mesh.bound = []; % clamped or simple
mesh.PointLoads = []; % in the center (or as close as poss.)
mesh.SurfLoads = []; % evenly distributed
% Name the mesh - with elements
mesh.filename = ['RectMeshQ9_' num2str(nelx) 'x' num2str(nely) ...
    '_E_' num2str(E) '_nu_' num2str(nu) '_rho_' num2str(rho) ...
    '_thk_' num2str(thk) '_' support ];
% Nodes
n=0;
dx=Lx/nelx/2;
dy=Ly/nely/2;
for nx=1:2*nelx+1
    for ny=1:2*nely+1
        n=n+1;
        mesh.X(n,1) = n;
        mesh.X(n,2) = (nx-1)*dx;
        mesh.X(n,3) = (ny-1)*dy;
    end
end

% Elements
e=0;
eActive = 0;
eBase = 0;
eLoad = [];
for elx=1:nelx
    for ely=1:nely
        e=e+1;
        n1 = 2*nny*(elx-1) + 2*ely-1;
        n2 = 2*nny*(elx)   + 2*ely-1;
        n3 = 2*nny*(elx)   + 2*ely+1;
        n4 = 2*nny*(elx-1) + 2*ely+1;
        n5 = nny*(2*elx-1) + 2*ely-1;
        n6 = 2*nny*(elx)   + 2*ely;
        n7 = nny*(2*elx-1) + 2*ely+1;
        n8 = 2*nny*(elx-1) + 2*ely;
        n9 = nny*(2*elx-1) + 2*ely;
        edof = [n1 n2 n3 n4 n5 n6 n7 n8 n9];
        mesh.IX(e,1)=e;
        mesh.IX(e,2:10)=edof;
        
        if ely> 0.5*nely
            %color = color+1;
            eActive = eActive +1;
            
            opt.topopt.indActive(eActive) = e;
            %             if rem(color,2)>0
            opt.topopt.x(e,1) = 1;
            %             else
            %                 opt.topopt.x(e,1) = 1;
            %             end
        else
            opt.topopt.x(e,1) = 0;
        end
        
        
        % fixed material ! only ONE !
        mesh.IX(e,11)=1;
    end
end
%% SET UP BOUNDARY CONDITIONS
leftdofs = 1:nny;
rightdofs = (1:nny)+nny*(nnx-1);
for i = 1:nnx
    %lowerdofs(i*3-2:i*3) = [1+nny*3*(i-1),2+nny*3*(i-1),3+nny*3*(i-1)];
    %upperdofs(i*3-2:i*3) =
    %[1+nny*3*(i-1)+(nely*q)*3,2+nny*3*(i-1)+(nely*q)*3,3+nny*3*(i-1)+(nely*q)*3];
    lowerdofs(i) = nny*(i-1)+1;
    upperdofs(i) = nny*i;
end
%fixeddofs = [leftdofs]; nfdofs = length(fixeddofs);
if strcmp(support,'capacitor')
    leftdofsDown = leftdofs(1:ceil(numel(leftdofs)/2)-1);
    leftdofsUp = leftdofs(ceil(numel(leftdofs)/2):end);
    rightdofsDown = rightdofs(1:ceil(numel(leftdofs)/2)-1);
    rightdofsUp = rightdofs(ceil(numel(leftdofs)/2):end);
    
    %mesh.bound(:,1) = fixeddofs; mesh.bound(1:nfdofs,2) = 1;
    %mesh.bound(nfdofs+1:2*nfdofs,2) = 2; mesh.bound(:,3) = 0;
    
    mesh.bound = [upperdofs',2*ones(length(upperdofs),1),zeros(length(upperdofs),1);
        lowerdofs',2*ones(length(lowerdofs),1),zeros(length(lowerdofs),1);
        leftdofs',ones(length(leftdofs),1),zeros(length(leftdofs),1);
        rightdofs',ones(length(rightdofs),1),zeros(length(rightdofs),1)];
    
   
        
    
   
        
%          mesh.bound = [upperdofs',2*ones(length(upperdofs),1),zeros(length(upperdofs),1);
%         lowerdofs',2*ones(length(lowerdofs),1),zeros(length(lowerdofs),1);
%         leftdofsDown',ones(length(leftdofsDown),1),zeros(length(leftdofsDown),1);
%         rightdofsDown',ones(length(rightdofsDown),1),zeros(length(rightdofsDown),1)];
    %keyboard
    %  mesh.bound =
    %  [leftdofs',ones(length(leftdofs),1),zeros(length(leftdofs),1);
    %  leftdofs',2*ones(length(leftdofs),1),zeros(length(leftdofs),1);]
    
elseif strcmp(support,'Beam')
    
    mesh.bound = [upperdofs',2*ones(length(upperdofs),1),zeros(length(upperdofs),1);
        lowerdofs',2*ones(length(lowerdofs),1),zeros(length(lowerdofs),1);
        leftdofs',ones(length(leftdofs),1),zeros(length(leftdofs),1);
        rightdofs',ones(length(rightdofs),1),zeros(length(rightdofs),1)];
    
 
    
end

structDofsQ9 = mesh.IX(1:end,2:10);
structDofsQ9 = unique(reshape(structDofsQ9,numel(structDofsQ9),1));
%opt.StructDofs = structDofsQ9;

ampl = 1e-4;
mesh.SuppLoadX = [];
mesh.SuppLoadY = [structDofsQ9,ampl*ones(length(structDofsQ9),1)];
%mesh.SuppLoadX = [];
%mesh.SuppLoadY = [];
% Pointload in the center node
%mesh.PointLoads = [1 2 -1000]; keyboard
mesh.PointLoads = [];
%mesh.PointLoads = [mesh.IX(eLoad(2),3) 2 1000]; p = 1e4; 
%opt.e_obj = eLoad';
mesh.SurfLoads = [];%[eLoad' ones(length(eLoad),1) p*ones(length(eLoad),1)];
%mesh.SurfLoads = [Base' 2*ones(length(Base),1) p*ones(length(Base),1)];
%mesh.wdof =  [nn 2 1];

mesh=MixedMesh_Q9toQ4(mesh,opt,support);
opt.objdispDOFs = [nny*nelx/2-(nely-1)];
%opt.objdispDOFs = [];
end

function [mesh] = MixedMesh_Q9toQ4(MESH,opt,support)

mesh.Q9 = MESH;
IX = mesh.Q9.IX;
X = mesh.Q9.X;
RemoveIndex=[];
mesh.Q4nodeIndex = [];

%% obtaining indeces for nodes 5 to 9
for i = 1:length(IX(:,1))
    RemoveIndex((end+1):end+5) =  IX(i,6:10);
    mesh.Q4nodeIndex((end+1):end+4) = IX(i,2:5);
end
% removing dublicates
RemoveIndex = unique(RemoveIndex);
mesh.Q4nodeIndex = unique(mesh.Q4nodeIndex);
mesh.Q4.Q4nodeIndex = unique(mesh.Q4nodeIndex);
%mesh.QnodeIndex = RemoveIndex;
%% Creating X matrix with new numbering
X_temp = X;
X_temp(RemoveIndex,:) = [];

mesh.Q4.X(:,1) = 1:length(X_temp(:,1));
mesh.Q4.X(:,2:3) = X_temp(:,2:3);
nnQ4 = mesh.Q4.X(end,1);

%% Substituting node numbering in IX with new numbering
IX_temp = IX(:,1:5);
mesh.Q4.IX(:,1) = IX_temp(:,1);

for i = 1:nnQ4
    IX_temp(IX_temp==X_temp(i,1)) = mesh.Q4.X(i,1);
end

mesh.Q4.IX(:,2:5) = IX_temp(:,2:5);
mesh.Q4.IX(:,6) = 1;



nn = length(mesh.Q4.X(:,1));
mesh.Q4.neqn = nn;

%% SET UP BOUNDARY CONDITIONS

nnx = mesh.Q9.nelx+1;
nny = mesh.Q9.nely+1;
leftdofs = 1:nny;
rightdofs = (1:nny)+nny*(nnx-1);
lowerdofs = nny.*([1:nnx]-1)+1
upperdofs = nny.*[1:nnx];
%for i = 1:nnx
%lowerdofs(i*3-2:i*3) = [1+nny*3*(i-1),2+nny*3*(i-1),3+nny*3*(i-1)];
%upperdofs(i*3-2:i*3) =
%[1+nny*3*(i-1)+(nely*q)*3,2+nny*3*(i-1)+(nely*q)*3,3+nny*3*(i-1)+(nely*q)*3];
%    lowerdofs(i) = nny*(i-1)+1;
%   upperdofs(i) = nny*i;
%end mesh.bound =
%[leftdofs',ones(length(leftdofs),1),zeros(length(leftdofs),1);
if strcmp(support,'capacitor')
mesh.Q4.bound = [upperdofs',ones(length(upperdofs),1),opt.phi(1)*ones(length(upperdofs),1);
    lowerdofs',ones(length(lowerdofs),1),0*ones(length(lowerdofs),1)];
%mesh.Q4.PointLoads = [floor(nn/2),1,100000];
p = 0.0001;
%mesh.Q4pres.bound = [lowerdofs',ones(length(lowerdofs),1),p*ones(length(lowerdofs),1)];
mesh.Q4pres.bound = []
%mesh.Q4pres.Press = [1 2];

elseif strcmp(support,'Beam')
    
end
mesh.Q4.PointLoads = [];

mesh.Q4pres.PointLoads = [];
%% SurfLoads = [e-number face pressure]
% Element faces:
%   3
% 4|_|2
%   1

%p = 1; mesh.Q4.SurfLoads =
%[(1:mesh.Q9.nely)',4*ones(mesh.Q9.nely,1),p*ones(mesh.Q9.nely,1)];
mesh.Q4.SurfLoads = [];
mesh.Q4.Material = mesh.Q9.Material;
mesh.Q4.absorbBC = [0];
mesh.Q4.acDom = [0];
% Finding boundary dofs for pressure dofs
%nnx = mesh.Q9.nelx+1; nny = mesh.Q9.nely+1; for i = 1:nnx
%    mesh.Q4.lowerdofs(i) = nny*(i-1)+1;
%   mesh.Q4.upperdofs(i) = nny*i;
%end
end



