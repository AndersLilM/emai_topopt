function [u,phi]=analyticRef(k,d0,eps);
% 1D example, analytic solution
%k = 100;eps = 1; d0 = 1;
u = 0:0.0001:0.33;
phi = sqrt(k*u.*(d0-u).^2*2*1/eps);
%(1/2)*phi^2*varepsilon/(k*d^2)
plot(phi/5.44,u/d0,'k')
set(gca,'Fontsize',17)
xlabel('\phi [V]')
ylabel('u [m]')