function opt = Assembly_electro(meshQ9,meshQ4,meshQ4pres,study,opt,NRit)
% Method that returns assemblies of K,M,C and D
% output: opt.K, opt.M, opt.C, opt.D, opt.Null, opt.P
% (Q9 and Q4 respectively)

%% Global numbers (for safe keeping)

opt.nel = size(meshQ9.IX,1);
opt.neqnQ9 = size(meshQ9.X,1)*2;
opt.neqnQ4 = size(meshQ4.X,1);


%% SUPPORTS (The N-Matrix)
%%%%%%%%%%%%% Q9
N = ones(opt.neqnQ9,1);
opt.gQ9=zeros(opt.neqnQ9,1);
for i=1:size(meshQ9.bound,1)
    N(2*meshQ9.bound(i,1)-(2-meshQ9.bound(i,2))) = 0;
    opt.gQ9(2*meshQ9.bound(i,1)-(2-meshQ9.bound(i,2))) = meshQ9.bound(i,3);
end
N9 = N;
opt.NullQ9 = spdiags(N,0,opt.neqnQ9,opt.neqnQ9);

%%%%%%%%%%%%% Q4 Electric!
N = ones(opt.neqnQ4,1);
opt.gQ4=zeros(opt.neqnQ4,1);
for i=1:size(meshQ4.bound,1)
    N(meshQ4.bound(i,1)) = 0;
    opt.gQ4(meshQ4.bound(i,1)) = meshQ4.bound(i,3);
end
N4 = N;

opt.NullQ4 = spdiags(N,0,opt.neqnQ4,opt.neqnQ4);

%%%%%%%%%%%%% Q4 Acoustic!
N = ones(opt.neqnQ4,1);
opt.gQ4pres=zeros(opt.neqnQ4,1);
for i=1:size(meshQ4pres.bound,1)
    N(meshQ4pres.bound(i,1)) = 0;
    opt.gQ4pres(meshQ4pres.bound(i,1)) = meshQ4pres.bound(i,3);
end
N4p = N;
%opt.PresQ4 = spdiags(opt.gQ4pres,0,opt.neqnQ4,opt.neqnQ4);
opt.NullQ4pres = spdiags(N,0,opt.neqnQ4,opt.neqnQ4);

% temp = [N4p;N9;N4]*0+1;
% temp(opt.absorbDofsQ4) = 0;
% temp(meshQ4.neqn+opt.absorbDofs*2) = 0;
%%%%%%%%%%%% GLOBAL
opt.NullGLOB = spdiags([N4p;N9;ones(length(N4p),1)],0,opt.neqnQ9+2*opt.neqnQ4,opt.neqnQ9+2*opt.neqnQ4);
opt.Nullstatic = spdiags([ones(length(N4p),1);N9;N4],0,opt.neqnQ9+2*opt.neqnQ4,opt.neqnQ9+2*opt.neqnQ4);
%opt.NullAbsorb = spdiags(temp,0,opt.neqnQ9+2*opt.neqnQ4,opt.neqnQ9+2*opt.neqnQ4);
%% VECTOR (LOADS) Q9
opt.PQ9 = zeros(opt.neqnQ9,1); % first column is for points, second for press.
for i=1:size(meshQ9.PointLoads,1)
    opt.PQ9(2*meshQ9.PointLoads(i,1)-(2-meshQ9.PointLoads(i,2)),1) = meshQ9.PointLoads(i,3);
end

% SurfaceLoads
for e=1:size(meshQ9.SurfLoads,1)
    nenQ9 = meshQ9.IX(meshQ9.SurfLoads(e,1),2:10);
    for i=1:9
        edof(2*i-1) = 2*nenQ9(i)-1;
        edof(2*i) = 2*nenQ9(i);
    end
    xp = meshQ9.X(nenQ9,2)';
    yp = meshQ9.X(nenQ9,3)';
    nface = meshQ9.SurfLoads(e,2);
    
    if nface == 1
        eta = [-1 -1 -1];
        xi = [-sqrt(0.6),0,sqrt(0.6)];
        w_G = [5/9 8/9 5/9];
    elseif nface == 2
        eta = [-sqrt(0.6),0,sqrt(0.6)];
        xi = [1 1 1];
        w_G = [5/9 8/9 5/9];
    elseif nface == 3
        eta = [1 1 1];
        xi = [-sqrt(0.6),0,sqrt(0.6)];
        w_G = [5/9 8/9 5/9];
    elseif nface == 4
        eta = [-sqrt(0.6),0,sqrt(0.6)];
        xi = [-1 -1 -1];
        w_G = [5/9 8/9 5/9];
    end
    matID = meshQ9.IX(e,11);
    t = meshQ9.Material(matID,1);
    for ii = 1:3
        [N,J]=shapeFuncQ9(xp,yp,eta(ii),xi(ii));
        J=[[-J(1,2);J(1,1)],[-J(2,2);J(2,1)],[J(1,2);-J(1,1)],[J(2,2);-J(2,1)]];
        
        opt.PQ9(edof,1) = opt.PQ9(edof,1) + N'*meshQ9.SurfLoads(e,3)*J(:,nface)*w_G(ii)*t;
    end
end
opt.PQ9surf = opt.PQ9;
opt.PQ9 = opt.PQ9*0;

%% VECTOR (LOADS) Q4
% PointLoads
opt.PQ4 = zeros(opt.neqnQ4,1); % first column is for points, second for press.
for i=1:size(meshQ4.PointLoads,1)
    opt.PQ4(meshQ4.PointLoads(i,1),1) = meshQ4.PointLoads(i,3);
end

%% VECTOR (LOADS) Q4 Acoustic!
% PointLoads
opt.PQ4pres = zeros(opt.neqnQ4,1); % first column is for points, second for press.
for i=1:size(meshQ4pres.PointLoads,1)
    opt.PQ4pres(meshQ4pres.PointLoads(i,1),1) = meshQ4pres.PointLoads(i,3);
end


%% Assembling [K],[M],[D],[C] %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% # element dof
ldofQ4=4;
ldofQ9=18;
edofQ9 = zeros(1,ldofQ9);

% Zero the arrays for the triplets
IK = zeros(opt.nel*ldofQ4*ldofQ9,1);
JK = zeros(opt.nel*ldofQ4*ldofQ9,1);
ID = zeros(opt.nel*ldofQ4*ldofQ9,1);
JD = zeros(opt.nel*ldofQ4*ldofQ9,1);
IC = zeros(opt.nel*ldofQ4*ldofQ9,1);
JC = zeros(opt.nel*ldofQ4*ldofQ9,1);
CE = zeros(opt.nel*ldofQ4*ldofQ9,1);
IKP = complex(zeros(opt.nel*ldofQ4*ldofQ9,1));
Kuu_m = zeros(opt.nel*ldofQ4*ldofQ9,1);
Kuu_e = zeros(opt.nel*ldofQ4*ldofQ9,1);
nNu = zeros(opt.nel*ldofQ4*ldofQ9,1);
Kee = zeros(opt.nel*ldofQ4*ldofQ9,1);
Kue = zeros(opt.nel*ldofQ4*ldofQ9,1);
Keu = zeros(opt.nel*ldofQ4*ldofQ9,1);
ME = zeros(opt.nel*ldofQ4*ldofQ9,1);
DE = zeros(opt.nel*ldofQ4*ldofQ9,1);
Psource = complex(opt.PQ9*0);
Q = zeros(opt.nel*ldofQ4*ldofQ9,1);
dQ = zeros(opt.nel*ldofQ4*ldofQ9,1);
ntripletsK = 0;
ntripletsD = 0;
ntripletsC = 0;

%% Find nodes in void domain
opt.Indx = zeros(size(meshQ9.X,1),1);
for ee=1:opt.nel
    nenQ9 = meshQ9.IX(ee,2:10);
    opt.Indx(nenQ9) = 1/4*[1,1,1,1,1,1,1,1,1]'*(opt.topopt.x(ee)>0.2)+opt.Indx(nenQ9) ;
end
opt.Indx = opt.Indx>=1/5;

% Loop over element and integrate
for e=1:opt.nel
    % element nodes
    nenQ4 = meshQ4.IX(e,2:5);
    
    nenQ9 = meshQ9.IX(e,2:10);
    
    % Get coordinates
    x.p = meshQ4.X(nenQ4,2)';
    y.p = meshQ4.X(nenQ4,3)';
    x.u = meshQ9.X(nenQ9,2)';
    y.u = meshQ9.X(nenQ9,3)';
    % Get element dofs
    
    edofQ4 = nenQ4;
    
    for i=1:9
        edofQ9(2*i-1) = 2*nenQ9(i)-1;
        edofQ9(2*i) = 2*nenQ9(i);
    end
    
    %% Get material parameters
    matID = meshQ9.IX(e,11);
    thk = meshQ9.Material(matID,1);        K   = opt.topopt.Kx(e);
    G   = opt.topopt.Gx(e);         rho   = opt.topopt.rhox(e);
    permg = opt.topopt.pxg(e); % General permmitivity
    permr = opt.topopt.pxr(e); % Permmitivity for maxwell tensor
    
    phi = opt.U(meshQ4.neqn+meshQ9.neqn+edofQ4);
    U = opt.U(meshQ4.neqn+edofQ9);
    
    %% Get the integrated element matrices. Structural only computes in first loop
    
    [kee0,kuu_e0,kue0,keu0,fe0] = electricElementMatrixFull(x,y,phi,U,thk,opt.nGauss,opt.Fflag);
    %[kee0,kuu_e0,kue0,keu0,fe0]=electricElementMatrixFullQ8(x,y,phi,U,thk,opt.nGauss,opt.Fflag);
    if e<2
        [kuu_m0,me0,De0,Ce,~,~,dQe] = ElasticElement_matrix(x,y,thk);
    end
    %mesh.Q4.absorbBC
    if sum(meshQ4.absorbBC(:,1)==e)
        eindex = find(meshQ4.absorbBC(:,1)==e);
        [nU, ikp, ikpIn] = AbsorbingBC(meshQ9,e,meshQ4.absorbBC(eindex,2));
        nU = nU*rho;
        %c = 1;
        c = (sqrt(K/rho));
        %ikp = ikp*sqrt(-1)*1/(sqrt(K/rho));
        ikp = ikp*sqrt(-1)*1/c;
        ikpIn = 2*ikpIn*sqrt(-1)*1/c*meshQ4.absorbBC(eindex,3);
    else
        nU = zeros(18,18);
        ikp = zeros(18,4);
        ikpIn = zeros(18,1);
    end
    
    %%% Remove forces in void domain
    indX=opt.Indx(nenQ9);
    indXdof = zeros(2*length(indX),1);
    index=find(indX>0);
    indXdof(2*index) = 1;
    indXdof(2*index-1) = 1;
    indXNull = diag(indXdof);
    fe0 = (indXNull*fe0')';
    kue0 = indXNull*kue0;
    kuu_e0 =indXNull'* kuu_e0;
    %%% slut
    
    kee = kee0.*permg;
    keu = keu0'.*permg;
    Fe = -fe0'.*permr;
    kue = kue0.*permr;
    kuu_e = kuu_e0.*permr;
    
    kuu_m = kuu_m0*G;
    De = De0*(1/K);
    q = De0;
    me =  me0*rho;
    
    opt.PQ9(edofQ9,1) = opt.PQ9(edofQ9,1)+Fe;
    Psource(edofQ9,1) = Psource(edofQ9,1)+ ikpIn;
    % add to global system (I,J,[KE,ME,DE,CE])
    for krow = 1:ldofQ9
        for kcol = 1:ldofQ9
            ntripletsK = ntripletsK+1;
            IK(ntripletsK) = edofQ9(krow);
            JK(ntripletsK) = edofQ9(kcol);
            Kuu_m(ntripletsK) = kuu_m(krow,kcol);
            Kuu_e(ntripletsK) = kuu_e(krow,kcol);
            nNu(ntripletsK) = nU(krow,kcol);
            ME(ntripletsK) = me(krow,kcol);
        end
    end
    
    for krow = 1:ldofQ4
        for kcol = 1:ldofQ4
            ntripletsD = ntripletsD+1;
            ID(ntripletsD) = edofQ4(krow);
            JD(ntripletsD) = edofQ4(kcol);
            DE(ntripletsD) = De(krow,kcol);
            if sum(meshQ4.acDom == e)
                Q(ntripletsD) = q(krow,kcol);
                dQ(ntripletsD) = dQe(krow,kcol);
            end
            Kee(ntripletsD) = kee(krow,kcol);
        end
    end
    
    for krow = 1:ldofQ9
        for kcol = 1:ldofQ4
            ntripletsC = ntripletsC+1;
            IC(ntripletsC) = edofQ9(krow);
            JC(ntripletsC) = edofQ4(kcol);
            CE(ntripletsC) = Ce(krow,kcol);
            Kue(ntripletsC) = kue(krow,kcol);
            Keu(ntripletsC) = keu(krow,kcol);
            IKP(ntripletsC) = ikp(krow,kcol); 
        end
    end
    
end
% Global load vector P
% indEl = find(opt.topopt.x>0.5);
%  Enodes = meshQ9.IX(indEl,2:10);
%  Enodes = reshape(Enodes,numel(Enodes),1);
%  Enodes = unique (Enodes);
%  Edofs = [Enodes*2;Enodes*2-1];
%  LL = zeros(numel(opt.PQ9),1);
%  LL (Edofs) = 1;
%  opt.PQ9 = opt.PQ9.*LL;
opt.P = [opt.PQ4pres;opt.PQ9;opt.PQ4];
opt.Psource = [opt.PQ4pres*0;Psource;opt.PQ4*0];



%% Constructing sparse matrices [Ku],[Mu],[D],[C]
ind = find(IK>0);
if NRit <= 1
    opt.Kuu_m = sparse(IK(ind),JK(ind),Kuu_m(ind),opt.neqnQ9,opt.neqnQ9);
    opt.nNu = sparse(IK(ind),JK(ind),nNu(ind),opt.neqnQ9,opt.neqnQ9);
end
opt.Kuu_e = sparse(IK(ind),JK(ind),Kuu_e(ind),opt.neqnQ9,opt.neqnQ9);
opt.Mu = sparse(IK(ind),JK(ind),ME(ind),opt.neqnQ9,opt.neqnQ9);


ind = find(ID>0);
opt.D = sparse(ID(ind),JD(ind),DE(ind),opt.neqnQ4,opt.neqnQ4);
opt.Q = sparse(ID(ind),JD(ind),Q(ind),opt.neqnQ4,opt.neqnQ4);
opt.dQ = sparse(ID(ind),JD(ind),dQ(ind),opt.neqnQ4,opt.neqnQ4);
opt.Kee = sparse(ID(ind),JD(ind),Kee(ind),opt.neqnQ4,opt.neqnQ4);
ind = find(IC>0);
opt.C = sparse(IC(ind),JC(ind),CE(ind),opt.neqnQ9,opt.neqnQ4);
opt.ikp = sparse(IC(ind),JC(ind),IKP(ind),opt.neqnQ9,opt.neqnQ4);
opt.Kue = sparse(IC(ind),JC(ind),Kue(ind),opt.neqnQ9,opt.neqnQ4);
opt.Keu = sparse(IC(ind),JC(ind),Keu(ind),opt.neqnQ9,opt.neqnQ4);

% Collecting into global matrices [K] & [M]
%opt.K = [opt.Ku,opt.C;opt.C',-opt.D];
%opt.Kt = [opt.Kuu_m+opt.Kuu_e,opt.Kue;opt.Keu',opt.Kee]; % Tangent matrix

%opt.K = [opt.Kuu_m,0*opt.Kue;0*opt.Kue',opt.Kee];        % System matrix
opt.K0 = [-opt.D,-opt.C',0*opt.D;
    -opt.C,opt.Kuu_m,0*opt.C;
    0*opt.D, 0*opt.C',speye(size(opt.Kee))];

opt.K = [-opt.D,-opt.C',0*opt.D;
    -opt.C,opt.Kuu_m,0*opt.C;
    0*opt.D, 0*opt.C',opt.Kee];
opt.Loss = [-opt.D*(sqrt(-1)*opt.LossCoeff),-0*opt.C',0*opt.D;
    -0*opt.C,opt.Kuu_m*sqrt(-1)*opt.LossCoeff,0*opt.C;
    0*opt.D, 0*opt.C',opt.Kee*0];

opt.Kt = [-opt.D,-opt.C',0*opt.D;
    -opt.C,opt.Kuu_m+opt.Kuu_e,opt.Kue;
    0*opt.D, opt.Keu',opt.Kee];
%opt.M = [opt.Mu,0*opt.C;0*opt.C',0*opt.D];
opt.M = [0*opt.D,0*opt.C',0*opt.D;
    0*opt.C,opt.Mu,0*opt.C;
    0*opt.D, 0*opt.C',0*opt.Kee];

opt.NU = [-opt.D*0,opt.C'*0,0*opt.D;
    opt.C*0,opt.nNu,0*opt.C;
    0*opt.D, 0*opt.C',opt.Kee*0];

opt.IKP = [-opt.D*0,transpose(opt.ikp)*0,0*opt.D;
    opt.ikp,opt.nNu*0,0*opt.C;
    0*opt.D, 0*opt.C',opt.Kee*0];



end
function [N,J]=shapeFuncQ4(x,y,eta,xi)
N = 1/4*[(1-xi)*(1-eta) (1+xi)*(1-eta) (1+xi)*(1+eta) (1-xi)*(1+eta)];
J = 1/4*[-(1-eta) (1-eta) (1+eta) -(1+eta);-(1-xi) -(1+xi) (1+xi) (1-xi)]*[x',y'];
end

function [N,J]=shapeFuncQ9(x,y,eta,xi)
%       Q9 shape functions
N9 = (-xi^2+1)*(-eta^2+1);
N5 = (1/2)*(-xi^2+1)*(1-eta)-(1/2)*N9;
N6 = (1/2)*(1+xi)*(-eta^2+1)-(1/2)*N9;
N7 = (1/2)*(-xi^2+1)*(1+eta)-(1/2)*N9;
N8 = (1/2)*(1-xi)*(-eta^2+1)-(1/2)*N9;
N1 = (1/4*(1-xi))*(1-eta)-1/2*(N8+N5)-(1/4)*N9;
N2 = (1/4*(1+xi))*(1-eta)-1/2*(N5+N6)-(1/4)*N9;
N3 = (1/4*(1+xi))*(1+eta)-1/2*(N6+N7)-(1/4)*N9;
N4 = (1/4*(1-xi))*(1+eta)-1/2*(N7+N8)-(1/4)*N9;
Ni = [N1,N2,N3,N4,N5,N6,N7,N8,N9];

% Shape function matrix
N = [eye(2)*Ni(1),eye(2)*Ni(2),eye(2)*Ni(3),eye(2)*Ni(4),eye(2)*Ni(5),eye(2)*Ni(6),eye(2)*Ni(7),eye(2)*Ni(8),eye(2)*Ni(9)];
J = 1/4*[-(1-eta) (1-eta) (1+eta) -(1+eta);-(1-xi) -(1+xi) (1+xi) (1-xi)]*[x(1:4)',y(1:4)'];
% Constructiong strain-disp matrix [B] [3x18]
end

function [nU, ikp, ikpIn] = AbsorbingBC(meshQ9,BCe,nface)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
nU = zeros(18,18);
ikp = zeros(18,4);
ikpIn = zeros(18,1);
for e = BCe
    nenQ9 = meshQ9.IX(e,2:10);
    %for i=1:9
    %    edof(2*i-1) = 2*nenQ9(i)-1;
    %    edof(2*i) = 2*nenQ9(i);
    %end
    xp = meshQ9.X(nenQ9,2)';
    yp = meshQ9.X(nenQ9,3)';
    %nface = 1;%meshQ9.SurfLoads(e,2);
   nGauss = 3;
p_G = [-sqrt(0.6),0,sqrt(0.6)];
w_G = [5/9 8/9 5/9];
    if nface == 1
        eta = [-1 -1 -1];
        xi = p_G;
    elseif nface == 2
        eta = p_G;
        xi = [1 1 1];
    elseif nface == 3
        eta = [1 1 1];
        xi = p_G;
    elseif nface == 4
        eta = p_G;
        xi = [-1 -1 -1];
    end
    matID = meshQ9.IX(e,11);
    t = meshQ9.Material(matID,1);
    for ii = 1:nGauss
        [Nu,J] = shapeFuncQ9(xp,yp,eta(ii),xi(ii));
        [Np] = shapeFuncQ4(xp,yp,eta(ii),xi(ii));
        J = -[[-J(1,2);J(1,1)],[-J(2,2);J(2,1)],[J(1,2);-J(1,1)],[J(2,2);-J(2,1)]];
        n = [[0;-1],[1;0],[0;1],[-1;0]];
        nU = nU + Nu'*J(:,nface)*n(:,nface)'*Nu*w_G(ii)*t;
        ikp = ikp + Nu'*J(:,nface)*Np*w_G(ii)*t;
        ikpIn = ikpIn + Nu'*J(:,nface)*w_G(ii)*t;
        %opt.PQ9(edof,1) = opt.PQ9(edof,1) + N'*meshQ9.SurfLoads(e,3)*J(:,nface)*w_G(ii)*t;
    end
end


    function [N,J]=shapeFuncQ4(x,y,eta,xi)
        N = 1/4*[(1-xi)*(1-eta) (1+xi)*(1-eta) (1+xi)*(1+eta) (1-xi)*(1+eta)];
        %J = 1/4*[-(1-eta) (1-eta) (1+eta) -(1+eta);-(1-xi) -(1+xi) (1+xi) (1-xi)]*[x',y'];
    end

    function [N,J]=shapeFuncQ9(x,y,eta,xi)
        %       Q9 shape functions
        N9 = (-xi^2+1)*(-eta^2+1);
        N5 = (1/2)*(-xi^2+1)*(1-eta)-(1/2)*N9;
        N6 = (1/2)*(1+xi)*(-eta^2+1)-(1/2)*N9;
        N7 = (1/2)*(-xi^2+1)*(1+eta)-(1/2)*N9;
        N8 = (1/2)*(1-xi)*(-eta^2+1)-(1/2)*N9;
        N1 = (1/4*(1-xi))*(1-eta)-1/2*(N8+N5)-(1/4)*N9;
        N2 = (1/4*(1+xi))*(1-eta)-1/2*(N5+N6)-(1/4)*N9;
        N3 = (1/4*(1+xi))*(1+eta)-1/2*(N6+N7)-(1/4)*N9;
        N4 = (1/4*(1-xi))*(1+eta)-1/2*(N7+N8)-(1/4)*N9;
        Ni = [N1,N2,N3,N4,N5,N6,N7,N8,N9];
        
        % Shape function matrix
        N = [eye(2)*Ni(1),eye(2)*Ni(2),eye(2)*Ni(3),eye(2)*Ni(4),eye(2)*Ni(5),eye(2)*Ni(6),eye(2)*Ni(7),eye(2)*Ni(8),eye(2)*Ni(9)];
        J = 1/4*[-(1-eta) (1-eta) (1+eta) -(1+eta);-(1-xi) -(1+xi) (1+xi) (1-xi)]*[x(1:4)',y(1:4)'];
        % Constructiong strain-disp matrix [B] [3x18]
    end
end

