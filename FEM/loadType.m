function [P] = loadType(opt,mesh)
% Returns load vector P for linear vibration analysis
%


Ptract = opt.P*0;Ppres = opt.P*0;Psupp = opt.P*0;

%% Dirichlet pressure condition
if ~isempty(mesh.Q4pres.bound)
g = 0*opt.P;
g(1:mesh.Q4.neqn) = opt.gQ4pres;
Ppres =  -opt.K*g;
Ppres(1:mesh.Q4.neqn) = opt.NullQ4pres(1:mesh.Q4.neqn)*Ppres(1:mesh.Q4.neqn)+opt.gQ4pres;
Ppres(mesh.Q4pres.bound(:,1)) = mesh.Q4pres.bound(:,3);
end


%% Traction forces
Ptract = opt.P*0;
Ptract(mesh.Q4.neqn+(1:mesh.Q9.neqn)) = opt.PQ9surf;

%% Support excitation
if ~isempty(mesh.Q9.SuppLoadX) || ~isempty(mesh.Q9.SuppLoadY)
w1 = zeros(opt.neqnQ9,1);
if ~isempty(mesh.Q9.SuppLoadX)
w1(mesh.Q9.SuppLoadX(:,1)*2-1) = mesh.Q9.SuppLoadX(:,2);
end
if ~isempty(mesh.Q9.SuppLoadY)
w1(mesh.Q9.SuppLoadY(:,1)*2) = mesh.Q9.SuppLoadY(:,2);
end

w1full = opt.P*0;
w1full(opt.neqnQ4+(1:opt.neqnQ9)) = w1;
Psupp = opt.M * w1full;
end

P = Ppres + Ptract + Psupp;

end

