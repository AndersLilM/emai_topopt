function [kuu_m,me,De,Ce,N,Np,dQe] = ElasticElement_matrix(x,y,t)
%% ke and De without multiplication of Bulk modulus K and shear modulus G factors, for easy
%% calculating of sensitivities in topopt.
kuu_m = zeros(18,18);
me = zeros(18,18);
De = zeros(4,4);
dQe = zeros(4,4);
Ce = zeros(18,4);



%% Matrix operators for constructing [Dd] [3x3]
I0 = diag(1/2*[2,2,1]);                         % Diagonal matrix
m = [1;1;0];
Dd = 2*(I0-1/2*(m)*m');                         % Mixed formulation D matrix

%E = K;
%Dd = 1/(1-nu^2)*[1 nu 0;nu 1 0;0 0 (1-nu)/2];


%% Gauss quadrature displacement field; Q9 "full"
nGauss = 3;
p_G = [-sqrt(0.6),0,sqrt(0.6)];
w_G = [5/9 8/9 5/9];

% w1 = (18+sqrt(30))/36;
% w2 = (18-sqrt(30))/36;
% p1 = sqrt(3/7-2/7*sqrt(6/5));
% p2 = sqrt(3/7+2/7*sqrt(6/5));
% nGauss = 4;
% p_G = [-p1 p1 p2 -p2];  
% w_G = [w1 w1 w2 w2];

% [ke] & [me] Numerical integration
for ii=1:nGauss
    xi=p_G(ii);
    for jj=1:nGauss
        eta=p_G(jj);
        [Bs,dJ,N] = shapeFuncQ9(x.p,y.p,eta,xi);
        kuu_m = kuu_m+Bs'*Dd*Bs*w_G(ii)*w_G(jj)*dJ*t;
        me = me+N'*N*w_G(ii)*w_G(jj)*dJ*t;
    end
end

if nargout>2
    %% Gauss quadrature pressure field and electric field; Q4 "full"
    nGauss = 2;
    p_G = [1/sqrt(3),-1/sqrt(3)];
    w_G = [1 1];

    
%      nGauss = 1;
%     p_G = [0,0];
%     w_G = [2];
%     w1 = (18+sqrt(30))/36;
% w2 = (18-sqrt(30))/36;
% p1 = sqrt(3/7-2/7*sqrt(6/5));
% p2 = sqrt(3/7+2/7*sqrt(6/5));
% nGauss = 4;
% p_G = [-p1 p1 p2 -p2];  
% w_G = [w1 w1 w2 w2];
    
    
    
    % [De] Numerical integration
    for ii=1:nGauss
        xi=p_G(ii);
        for jj=1:nGauss
            eta=p_G(jj);
            [Np,dJ,dNp] = shapeFuncQ4(x.p,y.p,eta,xi);
            De = De+Np'*Np*w_G(ii)*w_G(jj)*dJ*t;
            dQe = dQe+dNp'*dNp*w_G(ii)*w_G(jj)*dJ*t;
        end
    end
    

    
    %% Gauss quadrature Coupling. Using highest integration order.
nGauss = 3;
p_G = [-sqrt(0.6),0,sqrt(0.6)];
w_G = [5/9 8/9 5/9];


% nGauss = 2;
%     p_G = [1/sqrt(3),-1/sqrt(3)];
%     w_G = [1 1];
% w1 = (18+sqrt(30))/36;
% w2 = (18-sqrt(30))/36;
% p1 = sqrt(3/7-2/7*sqrt(6/5));
% p2 = sqrt(3/7+2/7*sqrt(6/5));
% nGauss = 4;
% p_G = [-p1 p1 p2 -p2];  
% w_G = [w1 w1 w2 w2];
    
    % [Ce] Numerical integration
    for ii=1:nGauss
        xi=p_G(ii);
        for jj=1:nGauss
            eta=p_G(jj);
            [Bs] = shapeFuncQ9(x.p,y.p,eta,xi);
            [Np,dJ] = shapeFuncQ4(x.p,y.p,eta,xi);
            Ce = Ce+Bs'*m*Np*w_G(ii)*w_G(jj)*dJ*t;
        end
    end
    
end

    function [Bs,detJ,N]=shapeFuncQ9(x,y,eta,xi)
            %       Q9 shape functions
            N9 = (-xi^2+1)*(-eta^2+1);
            N5 = (1/2)*(-xi^2+1)*(1-eta)-(1/2)*N9;
            N6 = (1/2)*(1+xi)*(-eta^2+1)-(1/2)*N9;
            N7 = (1/2)*(-xi^2+1)*(1+eta)-(1/2)*N9;
            N8 = (1/2)*(1-xi)*(-eta^2+1)-(1/2)*N9;
            N1 = (1/4*(1-xi))*(1-eta)-1/2*(N8+N5)-(1/4)*N9;
            N2 = (1/4*(1+xi))*(1-eta)-1/2*(N5+N6)-(1/4)*N9;
            N3 = (1/4*(1+xi))*(1+eta)-1/2*(N6+N7)-(1/4)*N9;
            N4 = (1/4*(1-xi))*(1+eta)-1/2*(N7+N8)-(1/4)*N9;
            Ni = [N1,N2,N3,N4,N5,N6,N7,N8,N9];
            %Ni = 1/4*[(1-xi)*(1-eta) (1+xi)*(1-eta) (1+xi)*(1+eta) (1-xi)*(1+eta)];
            % Shape function matrix
            N = [eye(2)*Ni(1),eye(2)*Ni(2),eye(2)*Ni(3),eye(2)*Ni(4),eye(2)*Ni(5),eye(2)*Ni(6),eye(2)*Ni(7),eye(2)*Ni(8),eye(2)*Ni(9)];
            %N = [eye(2)*Ni(1),eye(2)*Ni(2),eye(2)*Ni(3),eye(2)*Ni(4)];
        
        % Shape function derivatives
        N_deriv = [(1/4)*eta*(2*xi-1)*(eta-1), (1/4)*eta*(2*xi+1)*(eta-1), (1/4)*eta*(2*xi+1)*(1+eta), (1/4)*eta*(2*xi-1)*(1+eta), -eta*xi*(eta-1), -(1/2*(2*xi+1))*(eta^2-1), -eta*xi*(1+eta), -(1/2*(2*xi-1))*(eta^2-1),-2*xi*(-eta^2+1);
            (1/4)*xi*(xi-1)*(2*eta-1), (1/4)*xi*(1+xi)*(2*eta-1), (1/4)*xi*(1+xi)*(2*eta+1), (1/4)*xi*(xi-1)*(2*eta+1), -(1/2*(2*eta-1))*(xi^2-1), -eta*xi*(1+xi), -(1/2*(2*eta+1))*(xi^2-1), -eta*xi*(xi-1),-(2*(-xi^2+1))*eta];
        % Jacobi (N9 is not defining shape of element)
        % Inverse jacobi and determinant
        %N_deriv = 1/4*[-(1-eta) (1-eta) (1+eta) -(1+eta);-(1-xi) -(1+xi) (1+xi) (1-xi)];
        J = 1/4*[-(1-eta) (1-eta) (1+eta) -(1+eta);-(1-xi) -(1+xi) (1+xi) (1-xi)]*[x',y'];
        gamma = inv(J);
        detJ = det(J);
        gM = zeros(4);
        
        % Constructiong strain-disp matrix [B] [3x18]
        gM(1:2,1:2) = gamma;
        gM(3:4,3:4) = gamma;
        dN(1:2,2*[1:9]-1)=N_deriv(:,[1:9]);
        dN(3:4,2*[1:9])=N_deriv(:,[1:9]);
        Bu = gM*dN;
        Bs = [1 ,0 ,0 ,0;0 ,0 ,0, 1;0 ,1 ,1 ,0]*Bu;
    end

    function [N,detJ,dNy]=shapeFuncQ4(x,y,eta,xi)
        N = 1/4*[(1-xi)*(1-eta) (1+xi)*(1-eta) (1+xi)*(1+eta) (1-xi)*(1+eta)];
        J = 1/4*[-(1-eta) (1-eta) (1+eta) -(1+eta);-(1-xi) -(1+xi) (1+xi) (1-xi)]*[x',y'];
        dN = 1/4*[-(1-eta) (1-eta) (1+eta) -(1+eta);-(1-xi) -(1+xi) (1+xi) (1-xi)];
        dNy = 1/4*[-(1-xi) -(1+xi) (1+xi) (1-xi)];
        gamma = inv(J);
        detJ = det(J);
        %Be = gamma*1/4*[-(1-eta) (1-eta) (1+eta) -(1+eta);-(1-xi) -(1+xi) (1+xi) (1-xi)]   ;
    end

end
