function [E_f,dE_f,edofQ4]= ElecField(opt,mesh,elem)
%Tx = zeros(opt.nel,1);
%Ty = zeros(opt.nel,1);
meshQ4 = mesh.Q4;
meshQ9 = mesh.Q9;


eta = 0;
xi = 0;
for e=elem
    
    nenQ4 = meshQ4.IX(e,2:5);
    nenQ9 = meshQ9.IX(e,2:10);
    
    % Get coordinates
    x = meshQ4.X(nenQ4,2)';
    y = meshQ4.X(nenQ4,3)';
 
    % Get element dofs
    
    edofQ4 = nenQ4;
    for i=1:9
        edofQ9(2*i-1) = 2*nenQ9(i)-1;
        edofQ9(2*i) = 2*nenQ9(i);
    end
    
    phi = opt.DU(meshQ9.neqn+mesh.Q4.neqn+edofQ4);
    %U = opt.U(mesh.Q4.neqn+edofQ9);

dNN = 1/4*[-(1-eta) (1-eta) (1+eta) -(1+eta);-(1-xi) -(1+xi) (1+xi) (1-xi)];
        %4 node linear derivatives
        J = 1/4*[-(1-eta) (1-eta) (1+eta) -(1+eta);-(1-xi) -(1+xi) (1+xi) (1-xi)]*[x',y'];
        
        %Parametric mappng
        gamma = inv(J);
        
        %Derivatives and def. gradient F.
                %dNQ4 = 1/4*[-(1-eta) (1-eta) (1+eta) -(1+eta);-(1-xi) -(1+xi) (1+xi) (1-xi)];
                %Bu4(1:2,2*(1:4)-1)=dNQ4(:,(1:4));Bu4(3:4,2*(1:4))=dNQ4(:,(1:4));
                %du = [gamma,zeros(2);zeros(2),gamma]*Bu4*U(1:8);
                %F(1,1)=1+du(1);F(1,2)= du(2); F(2,1) = du(3); F(2,2)=1+du(4);
                %invF = inv(F);
                %detF = det(F);
        
        
                % Electric field and Maxwell tensor
        
                %E_f = -gamma*(invF)'*dNN*phi;
                E_f = -gamma*dNN*phi;
                dE_f = -gamma*dNN;
                %Te = 8.854e-12*[1/2*(E_f(1)^2-E_f(2)^2);1/2*(-E_f(1)^2+E_f(2)^2);E_f(1)*E_f(2)];
                
                %Tx(e) = Te(1);
                %Ty(e) = Te(2);
end