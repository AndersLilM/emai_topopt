function opt = Controller_Nonlinear(mesh,study,opt)
% Main driver for Nonlinear NR solver for electromechnical FEA followed by
% linear vibration analysis
%
% Input containers:
% mesh.X            = nodal coordinates
% mesh.IX           = topology table (and material index)
% mesh.Material:    = list of materials (e,[thk E nu dens])
% mesh.bound:       = list of constrained nodes (and their dof)
% mesh.PointLoads:  = list of loaded nodes (and their dof + load)+

% Find structural dofs. Not valid for topopt
jeps = find(opt.topopt.x==0);
structDofs = mesh.Q9.IX(jeps,2:10);
structDofs = unique(reshape(structDofs,numel(structDofs),1));

%% Start guess U_0, with BC fulfilled
if ~isempty(mesh.Q4.bound)
opt.U = opt.U*0;
opt.U(mesh.Q4.neqn+mesh.Q9.neqn+mesh.Q4.bound(:,1)) = mesh.Q4.bound(:,3);
end
MaxIt = 20;     % Max Newton iterations
omegaStep = 0;
flag = 1;       % if 1
%display('Solving..');
%display(['Max. NR iterations are: ' num2str(MaxIt)]);
tic
n = 0;
%Relnorm = 1;
for omega = study.omegaVec
    omegaStep = omegaStep + 1;
    lstep = 0; % Reset loadstep
    if flag %% flag, only do loadstep once
        %% NONLINEAR ANALYSIS
        for phi = opt.phi;
            
            %% update of loadstep
            if ~isempty(mesh.Q4.bound)
            mesh.Q4.bound(:,3) = (mesh.Q4.bound(:,3)>0)*phi;
            opt.U(mesh.Q4.neqn+mesh.Q9.neqn+mesh.Q4.bound(:,1)) = mesh.Q4.bound(:,3);
            end
            lstep = lstep+1;
            NRit = 0;
            Rnorm = 1e-4; % Convergence measure, size of the relative norm
            Relnorm = 1;
            while NRit<MaxIt
                NRit = NRit +1;
                study.omega = 0;
                %% Assembly System matrix (K) and tangent matrix (Kt)
                opt = Assembly_electro(mesh.Q9,mesh.Q4,mesh.Q4pres,study,opt,NRit);
                %% Compute residual
                R = (opt.K*opt.U-opt.P);
                %% Enforce BC on residual and tangent matrix
                R = opt.Nullstatic*R;
                % Compute relative norm for convergence criteria
                Relnorm = norm(R,2)/norm(opt.NullGLOB*opt.P);
                opt.Kt1 = opt.Kt;
                opt.Kt = opt.Nullstatic'*opt.Kt*opt.Nullstatic - ...
                    (opt.Nullstatic-speye(opt.neqnQ9+2*opt.neqnQ4,opt.neqnQ9+2*opt.neqnQ4));
                %% Loop is terminated if convergence crit. is reached, or zero potential.
                if Relnorm < Rnorm || sum(opt.phi)==0
                    break
                end
                %% Update of U
                dU = -(opt.Kt)\R;
                opt.U = opt.U+dU;
            end
            % save Residual in opt struct.
            opt.R = R;
            
            % Defining static displacement vector for plotting in run_
            % script. Maximum strucutural displacement in y-dir  
            vs = opt.U(mesh.Q4.neqn+structDofs*2);
            opt.uvecYmax(lstep) = max(abs(vs));
            opt.uvecOmegaYmax(omegaStep) =1;% max(abs(vs));
            
            %% Display loadstep info
            if NRit<MaxIt
                msg=(['Loadstep ' num2str(lstep) '/' num2str(numel(opt.phi)) ': Convergence criterion fulfilled' '  Iterations: ' num2str(NRit) '  Rel. Norm: ' num2str(Relnorm)]);
            else
                msg=(['Loadstep ' num2str(lstep) '/' num2str(numel(opt.phi)) ' Max iteration reached' '  Iterations: ' num2str(NRit) '  Rel. Norm: ' num2str(Relnorm)]);
            end
            fprintf('\n');
            fprintf(msg);
            fprintf('\n');
            
            
        end %% phi loop
        flag = 0;
        n = 0;
        fprintf('\n');
    end %% flag
    opt.DU = opt.U*0;
    %% LINEAR ANALYSIS
    if sum(study.omegaVec)>0 % Enters loop if excitation frequency is non zero
        
        %% BC on M and tangent matrix Kt
        opt.M = opt.NullGLOB'*opt.M*opt.NullGLOB;
        opt.KtLin = opt.NullGLOB'*opt.Kt*opt.NullGLOB- ...
            (opt.NullGLOB-speye(opt.neqnQ9+2*opt.neqnQ4,opt.neqnQ9+2*opt.neqnQ4));
        
        %opt.Kt = opt.NullAbsorb'*opt.Kt*opt.NullAbsorb;
       %opt.M = opt.NullAbsorb'*opt.M*opt.NullAbsorb;
        
        opt.K0 = opt.NullGLOB'*opt.K0*opt.NullGLOB - ...
            (opt.NullGLOB-speye(opt.neqnQ9+2*opt.neqnQ4,opt.neqnQ9+2*opt.neqnQ4));
        %% Creating loadvector for linear analysis
        P = loadType(opt,mesh);
        opt.Plin = P;
        %P = opt.NullAbsorb'*P;

        %% Solving for Linear vibration amplitudes DU
        opt.S = (-omega^2*opt.M+opt.KtLin+opt.Loss+1e9*opt.NU*omega^2+1e9*opt.IKP*omega);
        opt.DU = opt.S\(P+1e9*opt.Psource*omega);

        
        %opt.DU = (-omega^2*opt.M+opt.Kt+opt.NU*omega^2)\P;
        %opt.DU = (-omega^2*opt.M+opt.Kt)\P;
        if study.ZeroAnalysis == 1
            opt.S0 = (-omega^2*opt.M+opt.K0+opt.Loss+1e9*opt.NU*omega^2+1e9*opt.IKP*omega);
        opt.DU0 = opt.S0\(P+1e9*opt.Psource*omega);
        %opt.Ucomp = (-(omega)^2*opt.M+opt.K0)\P;
        end
        %opt.DU = DU;
        
        % returning objective values for plotting in run_ script.
        opt = plotDOF(opt,mesh,omegaStep,structDofs,study);
        
        if sum(study.omegaVec)>0
            fprintf(repmat('\b',1,n));
            msg = ['Frequency sweep: ' num2str(omegaStep) '/' num2str(numel(study.omegaVec))];
            fprintf(msg);
            n=numel(msg);
            
        end
    end
    
    
    
end %% omega loop
fprintf('\n');
toc
if opt.plotfields == 1
    if(any(findall(0,'Type','Figure')==2))
    close(1)
    close(2)
    end
    PlotVars(opt,mesh);
end
end

function opt = plotDOF(opt,mesh,omegaStep,structDofs,study)
if strcmp(opt.obj,'disp')
   if ~isempty(opt.objdispDOFs)
        vs = opt.DU(mesh.Q4.neqn+opt.objdispDOFs*2);
         opt.uvecOmegaYmax(omegaStep) = abs(vs);
    else
        % max deflection in y-dir
        vs = opt.DU(mesh.Q4.neqn+structDofs*2);
        opt.uvecOmegaYmax(omegaStep) = max(abs(vs));
        %opt.uvecOmegaYmax(omegaStep) = (abs(vs));
    end
elseif strcmp(opt.obj,'acoustic')
    %pobj = opt.DU(1:mesh.Q4.neqn);
   %pobj(opt.EDOFs) = opt.DU(opt.EDOFs);
    %vs = pobj'*opt.Q*pobj;
    %vs = DU(opt.objDOFs);
   c= csens_Pressure(opt,mesh,study,0);
    %[c] = csens_PressureComplx02(opt,mesh,study,0);
    opt.uvecOmegaYmax(omegaStep) = c;%sum(abs(vs));
elseif strcmp(opt.obj,'compliance')
    %Uc = 0*opt.DU;
    %Uc(mesh.Q4.neqn+(structDofsQ9)) = opt.DU(mesh.Q4.neqn+(structDofsQ9));
    c = csens_Compliance(opt,mesh,study,0);
    %vs = Uc'*(opt.Kt-omega^2*opt.M)*Uc;
    opt.uvecOmegaYmax(omegaStep) = c;
end
end



