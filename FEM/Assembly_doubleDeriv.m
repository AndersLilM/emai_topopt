function opt = Assembly_doubleDeriv(meshQ9,meshQ4,opt)
% Method that returns assemblies of K,M,C and D
% output: opt.K, opt.M, opt.C, opt.D, opt.Null, opt.P
% (Q9 and Q4 respectively)

%% Global numbers (for safe keeping)

opt.nel = size(meshQ9.IX,1);
opt.neqnQ9 = size(meshQ9.X,1)*2;
opt.neqnQ4 = size(meshQ4.X,1);





%% Assembling [K],[M],[D],[C] %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% # element dof
ldofQ4=4;
ldofQ9=18;
edofQ9 = zeros(1,ldofQ9);

% Zero the arrays for the triplets
IK = zeros(opt.nel*ldofQ4*ldofQ9,1);
JK = zeros(opt.nel*ldofQ4*ldofQ9,1);
ID = zeros(opt.nel*ldofQ4*ldofQ9,1);
JD = zeros(opt.nel*ldofQ4*ldofQ9,1);
IC = zeros(opt.nel*ldofQ4*ldofQ9,1);
JC = zeros(opt.nel*ldofQ4*ldofQ9,1);
%CE = zeros(opt.nel*ldofQ4*ldofQ9,1);
Ddf_ddphi = zeros(opt.nel*ldofQ4*ldofQ9,1);
Ddf_ddu = zeros(opt.nel*ldofQ4*ldofQ9,1);
Ddkee_ddu = zeros(opt.nel*ldofQ4*ldofQ9,1);
Ddkee_ddphi = zeros(opt.nel*ldofQ4*ldofQ9,1);

%opt.eArray.ddf_ddu = zeros(ldofQ9,ldofQ9,opt.nel);
%opt.eArray.ddf_ddphi = zeros(ldofQ9,ldofQ4,opt.nel);
%opt.eArray.ddkee_ddu = zeros(ldofQ4,ldofQ9,opt.nel);
%Kee = zeros(opt.nel*ldofQ4*ldofQ9,1);
%Kue = zeros(opt.nel*ldofQ4*ldofQ9,1);
%Keu = zeros(opt.nel*ldofQ4*ldofQ9,1);
%ME = zeros(opt.nel*ldofQ4*ldofQ9,1);
%DE = zeros(opt.nel*ldofQ4*ldofQ9,1);
%Q = zeros(opt.nel*ldofQ4*ldofQ9,1);
ntripletsK = 0;
ntripletsD = 0;
ntripletsC = 0;
Q4zero = sparse(opt.neqnQ4,opt.neqnQ4);
QCzero = sparse(opt.neqnQ4,opt.neqnQ9);
%% Find nodes in void domain
opt.Indx = zeros(size(meshQ9.X,1),1);
for ee=1:opt.nel
    nenQ9 = meshQ9.IX(ee,2:10);
    opt.Indx(nenQ9) = 1/4*[1,1,1,1,1,1,1,1,1]'*(opt.topopt.x(ee)>0.2)+opt.Indx(nenQ9) ;
end
opt.Indx = opt.Indx>= 1/5;

% Loop over element and integrate
for e=1:opt.nel
    % element nodes
    nenQ4 = meshQ4.IX(e,2:5);
    
    nenQ9 = meshQ9.IX(e,2:10);
    
    % Get coordinates
    x.p = meshQ4.X(nenQ4,2)';
    y.p = meshQ4.X(nenQ4,3)';
    x.u = meshQ9.X(nenQ9,2)';
    y.u = meshQ9.X(nenQ9,3)';
    % Get element dofs
    
    edofQ4 = nenQ4;
    
    for i=1:9
        edofQ9(2*i-1) = 2*nenQ9(i)-1;
        edofQ9(2*i) = 2*nenQ9(i);
    end
    
    %% Get material parameters
    matID = meshQ9.IX(e,11);
    thk = meshQ9.Material(matID,1);        %K   = opt.topopt.Kx(e);
    %G   = opt.topopt.Gx(e);         %rho   = opt.topopt.rhox(e);
    permg = opt.topopt.pxg(e); % General permmitivity
    permr = opt.topopt.pxr(e); % Permmitivity for maxwell tensor
    
    phiStatic = opt.U(meshQ4.neqn+meshQ9.neqn+edofQ4);
    UStatic = opt.U(meshQ4.neqn+edofQ9);
    philin = opt.DU(meshQ4.neqn+meshQ9.neqn+edofQ4);
    Ulin = opt.DU(meshQ4.neqn+edofQ9);
    
    %% Get the integrated element matrices. Structural only computes in first loop
    
    %[kee0,kuu_e0,kue0,keu0,fe0]=electricElementMatrixFull(x,y,phi,U,thk,opt.nGauss,opt.Fflag);
    [ddf_ddu,ddkee_ddu,ddf_ddphi,ddkee_ddphi] = doubleDeriv_ElectricElementMatrixFull(x,y,phiStatic,UStatic,Ulin,philin,thk,'reduced','on');
    %[kee0,kuu_e0,kue0,keu0,fe0]=electricElementMatrixFullQ8(x,y,phi,U,thk,opt.nGauss,opt.Fflag);
    %if e<2
    %    [kuu_m0,me0,De0,Ce] = ElasticElement_matrix(x,y,thk);
    %end
    
    %%% Remove forces in void domain
    indX=opt.Indx(nenQ9);
    indXdof = zeros(2*length(indX),1);
    index=find(indX>0);
    indXdof(2*index) = 1;
    indXdof(2*index-1) = 1;
    indXNull = diag(indXdof);
    %fe0 = (indXNull*fe0')';
    
    ddf_ddu = indXNull'*ddf_ddu;
    ddf_ddphi =indXNull* ddf_ddphi;
    
    %%% slut
    
    %opt.eArray.ddf_ddu(:,:,e) = ddf_ddu;
    %opt.eArray.ddf_ddphi(:,:,e) = ddf_ddphi;
    %opt.eArray.ddkee_ddu(:,:,e) = ddkee_ddu;
    
    %kee = kee0.*permg;
    %keu = keu0'.*permg;
    %Fe = -fe0'.*permr;
    %kue = kue0.*permr;
    %kuu_e = kuu_e0.*permr;
    
    %kuu_m = kuu_m0*G;
    %De = De0*(1/K);
    %q = De0;
    %me =  me0*rho;
    ddf_ddu = ddf_ddu*permr;
    ddf_ddphi = ddf_ddphi*permr;
    ddkee_ddu = transpose(ddkee_ddu)*permg;
    ddkee_ddphi = ddkee_ddphi*permg;
    %opt.PQ9(edofQ9,1) = opt.PQ9(edofQ9,1)+Fe;
    
    % add to global system (I,J,[KE,ME,DE,CE])
    for krow = 1:ldofQ9
        for kcol = 1:ldofQ9
            ntripletsK = ntripletsK+1;
            IK(ntripletsK) = edofQ9(krow);
            JK(ntripletsK) = edofQ9(kcol);
            Ddf_ddu(ntripletsK) = ddf_ddu(krow,kcol);
            %Kuu_e(ntripletsK) = kuu_e(krow,kcol);
            %ME(ntripletsK) = me(krow,kcol);
        end
    end
    
        for krow = 1:ldofQ4
            for kcol = 1:ldofQ4
                ntripletsD = ntripletsD+1;
                ID(ntripletsD) = edofQ4(krow);
                JD(ntripletsD) = edofQ4(kcol);
                Ddkee_ddphi(ntripletsD) = ddkee_ddphi(krow,kcol);
                %DE(ntripletsD) = De(krow,kcol);
                %Q(ntripletsD) = q(krow,kcol);
                %Kee(ntripletsD) = kee(krow,kcol);
            end
        end
    
    for krow = 1:ldofQ9
        for kcol = 1:ldofQ4
            ntripletsC = ntripletsC+1;
            IC(ntripletsC) = edofQ9(krow);
            JC(ntripletsC) = edofQ4(kcol);
            Ddf_ddphi(ntripletsC) = ddf_ddphi(krow,kcol);
            Ddkee_ddu(ntripletsC) = ddkee_ddu(krow,kcol);
            %CE(ntripletsC) = Ce(krow,kcol);
            %Kue(ntripletsC) = kue(krow,kcol);
            %Keu(ntripletsC) = keu(krow,kcol);
        end
    end
    
    
    
end
% Global load vector P
% indEl = find(opt.topopt.x>0.5);
%  Enodes = meshQ9.IX(indEl,2:10);
%  Enodes = reshape(Enodes,numel(Enodes),1);
%  Enodes = unique (Enodes);
%  Edofs = [Enodes*2;Enodes*2-1];
%  LL = zeros(numel(opt.PQ9),1);
%  LL (Edofs) = 1;
%  opt.PQ9 = opt.PQ9.*LL;
%opt.P = [opt.PQ4pres;opt.PQ9;opt.PQ4];




%% Constructing sparse matrices [Ku],[Mu],[D],[C]
ind = find(IK>0);
%if NRit <= 1;
%    opt.Kuu_m = sparse(IK(ind),JK(ind),Kuu_m(ind),opt.neqnQ9,opt.neqnQ9);
%end
opt.ddf_ddu = sparse(IK(ind),JK(ind),Ddf_ddu(ind),opt.neqnQ9,opt.neqnQ9);
%opt.Mu = sparse(IK(ind),JK(ind),ME(ind),opt.neqnQ9,opt.neqnQ9);


ind = find(ID>0);
opt.ddkee_ddphi = sparse(ID(ind),JD(ind),Ddkee_ddphi(ind),opt.neqnQ4,opt.neqnQ4);
%opt.D = sparse(ID(ind),JD(ind),DE(ind),opt.neqnQ4,opt.neqnQ4);
%opt.Q = sparse(ID(ind),JD(ind),Q(ind),opt.neqnQ4,opt.neqnQ4);
%opt.Kee = sparse(ID(ind),JD(ind),Kee(ind),opt.neqnQ4,opt.neqnQ4);
ind = find(IC>0);
opt.ddf_ddphi = sparse(IC(ind),JC(ind),Ddf_ddphi(ind),opt.neqnQ9,opt.neqnQ4);
opt.ddkee_ddu = sparse(IC(ind),JC(ind),Ddkee_ddu(ind),opt.neqnQ9,opt.neqnQ4);
%opt.Keu = sparse(IC(ind),JC(ind),Keu(ind),opt.neqnQ9,opt.neqnQ4);

% Collecting into global matrices [K] & [M]
%opt.K = [opt.Ku,opt.C;opt.C',-opt.D];
%opt.Kt = [opt.Kuu_m+opt.Kuu_e,opt.Kue;opt.Keu',opt.Kee]; % Tangent matrix

%opt.K = [opt.Kuu_m,0*opt.Kue;0*opt.Kue',opt.Kee];        % System matrix
opt.Ktt = [Q4zero,QCzero,Q4zero;
    QCzero',opt.ddf_ddu,opt.ddf_ddphi;
    Q4zero, transpose(opt.ddkee_ddu),opt.ddkee_ddphi];

%      opt.Kt = [-opt.D,opt.C',0*opt.D;
%          opt.C,opt.Kuu_m+opt.Kuu_e-study.omega^2*opt.Mu,opt.Kue;
%          0*opt.D, opt.Keu',opt.Kee];
% %opt.M = [opt.Mu,0*opt.C;0*opt.C',0*opt.D];
% opt.M = [0*opt.D,0*opt.C',0*opt.D;
%          0*opt.C,opt.Mu,0*opt.C;
%          0*opt.D, 0*opt.C',0*opt.Kee];
%


end
% function [N,J]=shapeFuncQ4(x,y,eta,xi)
% N = 1/4*[(1-xi)*(1-eta) (1+xi)*(1-eta) (1+xi)*(1+eta) (1-xi)*(1+eta)];
% J = 1/4*[-(1-eta) (1-eta) (1+eta) -(1+eta);-(1-xi) -(1+xi) (1+xi) (1-xi)]*[x',y'];
% end
%
% function [N,J]=shapeFuncQ9(x,y,eta,xi)
% %       Q9 shape functions
% N9 = (-xi^2+1)*(-eta^2+1);
% N5 = (1/2)*(-xi^2+1)*(1-eta)-(1/2)*N9;
% N6 = (1/2)*(1+xi)*(-eta^2+1)-(1/2)*N9;
% N7 = (1/2)*(-xi^2+1)*(1+eta)-(1/2)*N9;
% N8 = (1/2)*(1-xi)*(-eta^2+1)-(1/2)*N9;
% N1 = (1/4*(1-xi))*(1-eta)-1/2*(N8+N5)-(1/4)*N9;
% N2 = (1/4*(1+xi))*(1-eta)-1/2*(N5+N6)-(1/4)*N9;
% N3 = (1/4*(1+xi))*(1+eta)-1/2*(N6+N7)-(1/4)*N9;
% N4 = (1/4*(1-xi))*(1+eta)-1/2*(N7+N8)-(1/4)*N9;
% Ni = [N1,N2,N3,N4,N5,N6,N7,N8,N9];
%
% % Shape function matrix
% N = [eye(2)*Ni(1),eye(2)*Ni(2),eye(2)*Ni(3),eye(2)*Ni(4),eye(2)*Ni(5),eye(2)*Ni(6),eye(2)*Ni(7),eye(2)*Ni(8),eye(2)*Ni(9)];
% J = 1/4*[-(1-eta) (1-eta) (1+eta) -(1+eta);-(1-xi) -(1+xi) (1+xi) (1-xi)]*[x(1:4)',y(1:4)'];
% % Constructiong strain-disp matrix [B] [3x18]
% end


