#!/bin/sh

# -- Name of the job ---
#PBS -N EMAI_Prob1_Topopt



# - specify queue --
#PBS -q hpc
# -- estimated wall clock time (execution time): hh:mm:ss --
#PBS -l walltime= 72:00:00
#PBS -l feature=XeonE5-2680
# -number of processors/cores/nodes --
#PBS -l nodes=1:ppn=1
#PBS -l mem=25gb
#PBS -o $PBS_JOBNAME.$PBS_JOBID.out
#PBS -e $PBS_JOBNAME.$PBS_JOBID.err


# -- run in the current working (submission) directory --
if test X$PBS_ENVIRONMENT = XPBS_BATCH; then cd $PBS_O_WORKDIR; fi
# here follow the commands you want to execute

# Create a folder for the results
RESULTDIR=$PBS_O_WORKDIR/DATA_$PBS_JOBNAME\_$PBS_JOBID
mkdir $RESULTDIR


# Copy the matlab scripts to the RESULT_FOLDER
cp *.m $RESULTDIR/
cp -R FEM $RESULTDIR/
cp -R MESH $RESULTDIR/
cp -R TopOpt $RESULTDIR/
cp -R PLOT $RESULTDIR/




# Find the number of processors allocated for the job
NPROCS=`wc -l < $PBS_NODEFILE`
export fixed_allocation_of_np=$NPROCS

## Variables for matlab: frequency, filter type, filter radius, voltage, volume constraint
export omega=49.6e3
export filter=dens
export R=1
export phi=0.046
export volfrac=0.1



# Enter and run program
cd $RESULTDIR
matlab -nodisplay -r run_Problem1_topopt -logfile MatlabOut.out
exit
