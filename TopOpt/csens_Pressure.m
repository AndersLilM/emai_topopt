function [c,dc,opt] = csens_Pressure(opt,mesh,study,flag)
%Functin computes sensitivities (non-complex) with objective beeing the
%pressure field integratet over an abitrary domain.. c = P'*Q*P


P = zeros(size(opt.P));
P(opt.objDOFs) = opt.DU(opt.objDOFs);
Q = sparse(mesh.Q9.neqn+2*mesh.Q4.neqn,mesh.Q9.neqn+2*mesh.Q4.neqn);
Q(1:mesh.Q4.neqn,1:mesh.Q4.neqn) = opt.Q;

c = P'*Q*P;
ne = length(mesh.Q9.IX(:,1));
dc = zeros(ne,1);
if flag
    % Assembly of D/DS0(R1) and enforcing BC's
    opt = Assembly_doubleDeriv(mesh.Q9,mesh.Q4,opt);
    %S = opt.Kt-study.omega^2*opt.M;
    %S = opt.NullGLOB'*S*opt.NullGLOB - (opt.NullGLOB-speye(opt.neqnQ9+2*opt.neqnQ4,opt.neqnQ9+2*opt.neqnQ4));
    opt.Ktt = opt.NullGLOB'*opt.Ktt*opt.NullGLOB - (opt.NullGLOB-speye(opt.neqnQ9+2*opt.neqnQ4,opt.neqnQ9+2*opt.neqnQ4));
    
    %% Adjoint problems
    
    L = (2*Q*P);
    adjoint1 = opt.S'\(-L);
    adjoint0 = opt.Kt'\-(adjoint1'*opt.Ktt)';
    
    
    dc = zeros(ne,1);
    dc0_e = zeros(ne,1);
    dc0_s = zeros(ne,1);
    dc1_e = zeros(ne,1);
    dc1_s = zeros(ne,1);
    count = 0;
    for e = opt.topopt.indActive
        
        count = count +1;
        nenQ4 = mesh.Q4.IX(e,2:5);
        nenQ9 = mesh.Q9.IX(e,2:10);
        
        % Get coordinates
        xx.p = mesh.Q4.X(nenQ4,2)';
        yy.p = mesh.Q4.X(nenQ4,3)';
        xx.u = mesh.Q9.X(nenQ9,2)';
        yy.u = mesh.Q9.X(nenQ9,3)';
        
        edofQ4 = nenQ4;
        for i=1:9
            edofQ9(2*i-1) = 2*nenQ9(i)-1;
            edofQ9(2*i) = 2*nenQ9(i);
        end
        
        phiStatic = opt.U(mesh.Q9.neqn+mesh.Q4.neqn+edofQ4);
        UStatic = opt.U(mesh.Q4.neqn+edofQ9);
        
        [kee0,kuu_e0,kue0,keu0,fe0] = electricElementMatrixFull(xx,yy,phiStatic,UStatic,opt.topopt.thk,'reduced','on');
        %% Remove forces in void domain
        indX=opt.Indx(nenQ9);
        indXdof = zeros(2*length(indX),1);
        index=find(indX>0);
        indXdof(2*index) = 1;
        indXdof(2*index-1) = 1;
        indXNull = diag(indXdof);
        fe0 = (indXNull*fe0')';
        kue0 = indXNull*kue0;
        kuu_e0 =indXNull'* kuu_e0;
        %%% slut
        
        if count<2
            [Ku0,Me0,De0,~] = ElasticElement_matrix(xx,yy,opt.topopt.thk);
        end
        
        %% Sen dc/dx = D/Dx(c) + lam0*D/DX(R0) + lam1*D/DX(R1)  where x is the design variable and c is the objective function
        p1 = opt.DU(edofQ4);
        phi1 = opt.DU(mesh.Q9.neqn+mesh.Q4.neqn+edofQ4);
        U1 = opt.DU(mesh.Q4.neqn+edofQ9);
        
        p0 = opt.U(edofQ4);
        phi0 = opt.U(mesh.Q9.neqn+mesh.Q4.neqn+edofQ4);
        U0 = opt.U(mesh.Q4.neqn+edofQ9);
        
        lam1_p = adjoint1(edofQ4);
        lam1_u = adjoint1(opt.neqnQ4+edofQ9);
        lam1_e = adjoint1(opt.neqnQ9+opt.neqnQ4+edofQ4);
        lam0_p = adjoint0(edofQ4);
        lam0_u = adjoint0(opt.neqnQ4+edofQ9);
        lam0_e = adjoint0(opt.neqnQ9+opt.neqnQ4+edofQ4);
        
        
        %----- Sens from analysis 1 (linear vibration)
        
        % Structural sensitivities
        dc1_s(e) = opt.topopt.dGx(e)*(lam1_u'*Ku0*U1)-study.omega^2*opt.topopt.drhox(e)*(lam1_u'*Me0*U1)-opt.topopt.dKx(e)*lam1_p'*De0*p1;
        
        % Electric sensitivities
        dc1_e(e) = opt.topopt.dpxr(e)*(lam1_u'*kuu_e0*U1)+opt.topopt.dpxg(e)*(lam1_e'*kee0*phi1)+opt.topopt.dpxg(e)*(lam1_e'*keu0*U1)+...
            opt.topopt.dpxr(e)*(lam1_u'*kue0*phi1);
        
        
        %----- Sens from analysis 0 (nonlinear)
        % Structural sensitivities
        dc0_s(e) = opt.topopt.dGx(e)*(lam0_u'*Ku0*U0)-opt.topopt.dKx(e)*lam0_p'*De0*p0;
        % Electric sensitivities
        dc0_e(e) = opt.topopt.dpxr(e)*(lam0_u'*fe0')+opt.topopt.dpxg(e)*(lam0_e'*kee0*phi0);
        
        %----- Total sens
        dc(e) = dc0_s(e)+dc0_e(e)+dc1_s(e)+dc1_e(e);
        
    end
    
    
end


