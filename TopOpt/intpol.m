function [opt] = intpol(opt,flag)
% Topology optimization interpolation schemes
% RAMP is used for the stiffness parameters, SIMP for the electric
% parameters and the density is inpolated linearly

xPhys = opt.topopt.xPhys;
n = opt.topopt.n; % RAMP interpolation parameter
p = opt.topopt.p; % SIMP interpolation parameter  

% Defining material upper and lower bounds
Ks = opt.topopt.Kvec(1); Ka = opt.topopt.Kvec(2);
Gs = opt.topopt.Gvec(1); Ga = opt.topopt.Gvec(2);
psg = opt.topopt.permvecg(1); pag = opt.topopt.permvecg(2);
psr = opt.topopt.permvecr(1); par = opt.topopt.permvecr(2);
rhos = opt.topopt.rhovec(1);rhoa = opt.topopt.rhovec(2);




%% Interpolation
%RAMP K and G
opt.topopt.Kx = Ks.*xPhys./(1+(1-xPhys).*n)+Ka.*(1-xPhys./(1+(1-xPhys).*n));
opt.topopt.Gx = Gs.*xPhys./(1+(1-xPhys).*n)+Ga.*(1-xPhys./(1+(1-xPhys).*n));

% SIMP permmitivity (General and Relative)
opt.topopt.pxr = par + xPhys.^p.*(psr-par);
opt.topopt.pxg = pag + xPhys.^p.*(psg-pag);

% Linear rho
opt.topopt.rhox = rhoa+xPhys.^1.*(rhos-rhoa);

%% Derivatives
opt.topopt.dGx = -(Ga.*n-Gs.*n+Ga-Gs)./(n*xPhys-n-1).^2;
opt.topopt.dKx = (-Ks+Ka)*(n+1)./((Ka*n*xPhys-Ka*n+Ka*xPhys-Ks*xPhys-Ka).^2);

opt.topopt.dpxr = p*xPhys.^(p-1).*(psr-par);
opt.topopt.dpxg = p*xPhys.^(p-1).*(psg-pag);

opt.topopt.drhox = 1*xPhys.^(1-1).*(rhos-rhoa);






end