function [opt] = SetupFilter(opt,mesh)
% Setup H matrix for density filter
aa = opt.Lx/opt.nelx/2;
bb = opt.Ly/opt.nely/2;
neActive = numel(opt.topopt.indActive);
centercoordX = mesh.Q9.X(mesh.Q9.IX(opt.topopt.indActive,2),2)+aa;
centercoordY = mesh.Q9.X(mesh.Q9.IX(opt.topopt.indActive,2),3)+bb;
opt.topopt.H = sparse(neActive,neActive);
for i = 1:neActive;
    radii = sqrt((centercoordX-centercoordX(i)).^2+(centercoordY-centercoordY(i)).^2);
    Ne = find(radii<=opt.topopt.R);
    w = opt.topopt.R-radii(Ne);
    opt.topopt.H(i,Ne) = w;
end
    opt.topopt.Hs = sum(opt.topopt.H,2);
end

