function [xnew,opt] = MMA(opt,itr,c,dc,g,dg)
%keyboard
if itr == 1
    
    % Create the mma struct in opt
    opt.mma.n = size(dc,1);
    opt.mma.m = length(g);
    opt.mma.a0 = 1;
    opt.mma.a = 0 * ones(opt.mma.m,1);
    opt.mma.c = 1000 * ones(opt.mma.m,1);
    opt.mma.d = 0 * ones(opt.mma.m,1);
    if myIsField(opt.topopt,'movelimit')
        opt.mma.move = opt.topopt.movelimit;
    else
        opt.mma.move = 0.2;
    end
    opt.mma.low = 0; % not needed to be initialized !
    opt.mma.upp = 1; % not needed to be initialized !
    opt.mma.xold1 = opt.topopt.x(opt.topopt.indActive);
    opt.mma.xold2 = opt.topopt.x(opt.topopt.indActive);
    opt.mma.xmin = max(0,opt.topopt.x(opt.topopt.indActive)-opt.mma.move);
    opt.mma.xmax = min(1,opt.topopt.x(opt.topopt.indActive)+opt.mma.move);
    
    % Call MMA - note that all lag.mult. and slack variables are unused
    [xnew,opt.mma.intvars.ymma,opt.mma.intvars.zmma,opt.mma.intvars.lam,opt.mma.intvars.xsi,opt.mma.intvars.eta,...
        opt.mma.intvars.mu,opt.mma.intvars.zet,opt.mma.intvars.s,opt.mma.low,opt.mma.upp] = ...
        mmasub(opt.mma.m,opt.mma.n,itr,opt.topopt.x(opt.topopt.indActive),opt.mma.xmin,opt.mma.xmax,opt.mma.xold1,opt.mma.xold2, ...
        c,dc(:),0*dc(:),g,dg,0*dg,opt.mma.low,opt.mma.upp,opt.mma.a0,opt.mma.a,opt.mma.c,opt.mma.d);
    %z = opt.mma.intvars.zmma;
    % Save xold's
    opt.mma.xold2 = opt.mma.xold1;
    opt.mma.xold1 = opt.topopt.x(opt.topopt.indActive);
    % the final substitution is performed in the optimizer to fit with
    % the OC call
    
else
    opt.mma.move = opt.topopt.movelimit;
    opt.mma.xmin = max(0,opt.topopt.x(opt.topopt.indActive)-opt.mma.move);%0.001*ones(opt.mma.n,1);%
    opt.mma.xmax = min(1,opt.topopt.x(opt.topopt.indActive)+opt.mma.move);%ones(opt.mma.n,1);%
    
    % Call MMA - note that all lag.mult. and slack variables are unused
    [xnew,opt.mma.intvars.ymma,opt.mma.intvars.zmma,opt.mma.intvars.lam,opt.mma.intvars.xsi,opt.mma.intvars.eta,...
        opt.mma.intvars.mu,opt.mma.intvars.zet,opt.mma.intvars.s,opt.mma.low,opt.mma.upp] = ...
        mmasub(opt.mma.m,opt.mma.n,itr,opt.topopt.x(opt.topopt.indActive),opt.mma.xmin,opt.mma.xmax,opt.mma.xold1,opt.mma.xold2, ...
        c,dc(:),0*dc(:),g,dg,0*dg,opt.mma.low,opt.mma.upp,opt.mma.a0,opt.mma.a,opt.mma.c,opt.mma.d);
    %z = opt.mma.intvars.zmma;
    % Save xold's
    opt.mma.xold2 = opt.mma.xold1;
    opt.mma.xold1 = opt.topopt.x(opt.topopt.indActive);
    % the final substitution is performed in the optimizer to fit with
    % the OC call
end

end
