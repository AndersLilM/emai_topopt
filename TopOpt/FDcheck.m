function [ ] = FDcheck(mesh,study,opt,x0,c0,dc0,g,dg,intpmethod,FDelement)
% Finite difference check of sensitivities


nely = mesh.Q9.nely;
nelx = mesh.Q9.nelx;
ne = nely*nelx;                                          % number of elements


subplot(2,1,2)
opt.topopt.x(FDelement)= 0.75;
xplot = reshape(opt.topopt.x,nely,nelx);

colormap(jet(4))
labels = {'Passive1','FD checked','Active','Passive0'};
lcolorbar(labels,'fontweight','bold');
imagesc(-xplot); axis equal; axis tight;set(gca,'YDir','normal'); axis off;pause(1e-30);

% element number for sensitivity check
pertub =[1e-2,1e-3,1e-4,1e-5,1e-6,1e-7];                                            % pertubations
%pertub =[1e-6,1e-7,1e-8,1e-9,1e-10];

dg0 = 0*dc0;
dg0(opt.topopt.indActive) = dg;
for j = 1:length(FDelement)
    enum = FDelement(j);
    % unit vector
    ee = zeros(ne,1);
    ee(enum)=1;
    for i=1:length(pertub)
        % the Pertubated x
        opt.topopt.x = x0+ee*pertub(i);
        
        if strcmp(opt.Filt,'dens')
            opt.topopt.x(opt.topopt.indActive) = opt.topopt.H*opt.topopt.x(opt.topopt.indActive)./opt.topopt.Hs;
        end
        opt.topopt.xPhys = opt.topopt.x;
                   
            opt =intpol(opt,intpmethod);
            % FE analysis
            opt = Controller_Nonlinear(mesh,study,opt);
            % Objective function
            [c,~] = csens_PressureComplx02(opt,mesh,study,0);
            c = -c*opt.topopt.cScale;
            dcFD = (c-c0)/pertub(i); % Finite difference sensitivity
            dcFDvec(i) = abs(dcFD);
            %Volume
            gFD = sum(opt.topopt.xPhys(opt.topopt.indActive))./(opt.topopt.volfrac*numel(opt.topopt.indActive))-1;
            dgFD = (gFD-g)./pertub(i); % Finite difference sensitivity
            
            fprintf('Element: %i h: %e, dc0: %e, dcFD: %e, dg0: %e, dgFD: %e\n',j,pertub(i),dc0(enum),dcFD,dg0(enum),dgFD);
            frel(i,j) = abs((dcFD-dc0(enum))./dcFD);
            
            
        end
    end
    
    subplot(2,1,1)
    loglog(pertub,frel)
    %axis([min(pertub) max(pertub) 0 max(frel)]);
    axis tight
    set(gca,'Fontsize',16)
    xlabel('pertubation')
    ylabel('rel. error')
    
    subplot(2,1,2)
    opt.topopt.x(FDelement)= 0.75;
    xplot = reshape(opt.topopt.x,nely,nelx);
    
    colormap(jet(4))
    labels = {'Passive1','FD checked','Active','Passive0'};
    lcolorbar(labels,'fontweight','bold');
    imagesc(-xplot); axis equal; axis tight;set(gca,'YDir','normal'); axis off;pause(1e-30);
    
    figure
    loglog(pertub,dcFDvec)
    xlabel('pertubation')
    ylabel('d\phi/d\mu FD')
    
    pause(1)
    ansdlg = questdlg('Continue script?');
    
    
    
    if strcmp(ansdlg,'No') || strcmp(ansdlg,'Cancel')
        error('Script terminated by user')
    end
    
    
    close all
    
    
    
    
end

