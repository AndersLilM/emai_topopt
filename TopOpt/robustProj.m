function [xPhys,dxPhys] =robustProj(x0,eta,beta)
% Treshold projection for robust topology optimization.
% (Wang 2010) equation (9)
% eta = treshold parameter
% beta = projection parameter
xPhys=(tanh(beta*eta)+tanh(beta*(x0-eta)))/(tanh(beta*eta)+tanh(beta*(1-eta)));
dxPhys = beta*(1-tanh(beta*(x0-eta)).^2)/(tanh(beta*eta)+tanh(beta*(1-eta)));
end