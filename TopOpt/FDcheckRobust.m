function [ ] = FDcheckRobust(mesh,study,opt,x0,g,dg,beta,intpmethod,FDelement)
% Finite difference check of sensitivities
nely = mesh.Q9.nely;
nelx = mesh.Q9.nelx;
ne = nely*nelx;                                          % number of elements
%FDelement = [1 floor(ne/2) floor(ne-mesh.Q9.nely/2)];                                                  % element number for sensitivity check
pertub =[1e-1,1e-2,1e-3,1e-4,1e-5,1e-6,1e-7,1e-8, 1e-9 ];                                            % pertubations
%dczero = zeros(ne,1);
%dczero(opt.topopt.indActive) = dc0;
%dc0 = dczero;

frel = zeros(5,length(pertub),length(FDelement));

gFD = 0*g;

indEta = [1-0.40 0.5 0.40];

for j = 1:length(FDelement)
    enum = FDelement(j);
    % unit vector
    ee = zeros(ne,1);                                                         
    ee(enum)=1;
    enum = find(opt.topopt.indActive == FDelement(j));
    for i=1:length(pertub)
        % the Pertubated x
        opt.topopt.x = x0+ee*pertub(i);  
        
        for ii=1:3
            % Filtering
            xfilt = opt.topopt.x*0;
            xfilt(opt.topopt.indActive) = opt.topopt.H*opt.topopt.x(opt.topopt.indActive)./opt.topopt.Hs;
            
            
            
            % Projection
            [opt.topopt.xPhys,~] = robustProj(xfilt,indEta(ii),beta);
            
            
            % Interpolation to physical x
            opt =intpol(opt,intpmethod);
            
            % FE analysis
            opt = TopController(mesh,study,opt);
            
            % Objective functions
            [c,~,cc,~] = csens_BOX(opt,mesh,study);
            
            % Scale
             c = c*opt.topopt.cScale;
             cc = cc*opt.topopt.ccScale;
             
            % Compliance
            gFD(ii+1) = c;
            
            % Pressure
            
            if (ii==2)
                gFD(5) = cc - opt.topopt.ccon*opt.topopt.ccScale;
                %keyboard
            end
            
            % Volume
            if ii==3
                gFD(1) = sum(opt.topopt.xPhys(opt.topopt.indActive,1))./(opt.topopt.vfrac_d*numel(opt.topopt.indActive))-1;
            end
            
        end
        
        dgFD = (gFD-g)./pertub(i); % Finite difference sensitivity
        
        fprintf('Element: %i h: %e, ',j,pertub(i));
        for k=1:5
            fprintf('dg(%i): %e, dgFD(%i): %e ',k,dg(k,enum),k,dgFD(k))
            
        end
        fprintf('\n');
        
        
        frel(:,i,j) = abs((dgFD-dg(:,enum))./dgFD);
        
    end
    fprintf('\n');
end

%keyboard

%subplot(2,1,1)
%loglog(pertub,frel)
%axis([min(pertub) max(pertub) 0 max(frel)]);
axis tight
set(gca,'Fontsize',16)
xlabel('pertubation')
ylabel('d\phi/d\mu FD')
%legend('Corner element','Interior element','Boundary element')

%subplot(2,1,2)
 opt.topopt.x(FDelement)=opt.topopt.volfrac/3;
 xplot = reshape(opt.topopt.x,opt.nely,opt.nelx);
 
 %colormap(gray);
 colormap(jet(3))
    labels = {'Passive','Active','FD checked'};
    lcolorbar(labels,'fontweight','bold');
    imagesc(-xplot); axis equal; axis tight;set(gca,'YDir','normal'); axis off;pause(1e-30);
pause(1)
ansdlg = questdlg('Continue script?');


if strcmp(ansdlg,'No') || strcmp(ansdlg,'Cancel')
    error('Script terminated by user')
end


close all

end