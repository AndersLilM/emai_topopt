clear
close all
% 1D reference example

%% addpath to FEA, MESH and VISUALIZATION (ParaView)
addpath('FEM')
addpath('MESH')
addpath('PLOT')
addpath('TopOpt/MMA')
addpath('TopOpt')




%% Material properties
opt.plotMesh = 'k';
opt.plotfields = 1;
% Properties of the solid material
Es=100;
rhos = 1*(1/400)*Es*pi^2;
opt.topopt.nu=0.0;
opt.topopt.thk=1;

Ks = Es/(2*(1-opt.topopt.nu));                      % Bulk modulus 2D plane stress
%Ks = Es/(2*(1+opt.topopt.nu)*(1-2*opt.topopt.nu)); % Bulk modulus 2D plane strain
Gs = Es/(2*(1+opt.topopt.nu));                      % Shear modulus

%Properties of acoustic material
Ka = 1e-10;%1.42e5;%Ks*1e-10;%1%1.01325e2;%28.5e9;
rhoa = 1e-10;%1.293e5;%1e-10*rhos;%
Ga = 1e-10;
eps0 =1;
epsS = 1*eps0;
epsG = 10^5*eps0;
epsA = eps0;
%perma = 1%8.85e-12;
opt.obj = 'disp';
study.ZeroAnalysis = 0;
opt.LossCoeff = 0; %% Loss coefficient eta
%opt.phi = [0.1:0.9:21]
%% FE Analysis
opt.nGauss = 'reduced'
study.omegaVec = [0.01:0.232:15] %[0:0.232:15];
opt.Fflag = 'on';
% The 9.5 refers to scaling - i.e. 9.5 = 95%
opt.phi =  [0.544/2:0.544:0.544*9,5.0592,0.544*9.5]%,0.544*9.8,0.544*9.9]%,0.544*9.9];%0.544*9.8,0.544*9.95,0.544*10];
opt.phi =  linspace(0.27, 5.20,10);

%opt.phi = 0%[1:0.544/2:14]
% Material vector used for RAM
opt.topopt.Kvec = [Ks Ka];                                     
opt.topopt.Gvec = [Gs Ga];                                     
opt.topopt.rhovec = [rhos rhoa];   
opt.topopt.permvecg = [epsG epsA];
opt.topopt.permvecr = [epsS eps0];
%opt.topopt.permvec = [perma*10^5 perma]

%keyboard
%% Support & Mesh
opt.Lx=1;
opt.Ly=2;
opt.nelx=1*10;
opt.nely=2*10;
opt.ne = opt.nelx*opt.nely;
%support='MBB';
support='capacitor';
%[mesh.Q9, opt] = MBB2_StructMeshGenerator_Q9(Lx,Ly,nelx,nely,0,0,0,opt.topopt.thk,support,opt);
[mesh, opt] = ElectricMeshGenerator_Q9(opt.Lx,opt.Ly,opt.nelx,opt.nely,0,0,0,opt.topopt.thk,support,opt);
opt.U(:,1) = zeros(mesh.Q9.neqn+2*mesh.Q4.neqn,1);
display(['DOFS: ' num2str(mesh.Q9.neqn+2*mesh.Q4.neqn)]);
%mesh = MixedMesh_Q9toQ4(mesh);
%keyboard
xPlot = reshape(opt.topopt.x(:,1),opt.nely,opt.nelx);
colormap(gray);
    %subplot(2,3,1);
    %imagesc(-[xPlot]); axis equal; axis tight;set(gca,'YDir','normal');pause(1e-30);axis off;

opt.topopt.n = 1;
opt.topopt.p = 1;
opt.topopt.xPhys = opt.topopt.x;
[opt] = intpol(opt,'RAMP');



%% initilization TopOpt
%keyboard
 opt = Controller_Nonlinear(mesh,study,opt);
%BCcheck(opt,mesh)
 
figure(4)
[anu,anphi]=analyticRef(Es,opt.Ly/2,opt.topopt.permvecg(2));
%comsol=load('comsol_spring4mFine.txt');
hold on
plot(opt.phi/5.44,opt.uvecYmax/(opt.Ly/2),'o')
set(gca,'Fontsize',17)
%plot(comsol(:,1),comsol(:,2)/(opt.Ly/2));

xlabel('$\frac{\phi}{\phi_{PI}}$','interpreter','Latex','Fontsize',25)
ylabel('$\frac{x}{d_0}$','interpreter','Latex','Fontsize',25)
h=legend('Analytical solution','Electromechanical-Acoustic system','location','northwest');
set(h,'Interpreter','latex');
sti='/Users/Anders/Dropbox/DTU/11.Speciale/Thesis/Sections/FEM/Verification/';
%pdfPrint('MonolitVeri1D',sti)
comint = interp1(anphi,anu,opt.phi);

relerr = (comint-opt.uvecYmax)./comint;
figure(5)

plot(opt.phi,abs(relerr));
set(gca,'Fontsize',17)
%axis([0 6 0 0.01])
ylabel('rel. error')
xlabel('\phi [V]')

figure(6)
%[anu,anphi]=analyticRef(Es,opt.Ly/2,opt.topopt.permvecg(2));
%comsol=load('comsol_spring4mFine.txt');

%openfig('freq.fig');
u = forcedResp1D(study.omegaVec,opt.uvecYmax(end),opt.phi(end));
h1=semilogy(study.omegaVec/10,abs(opt.uvecOmegaYmax)/max(abs(opt.uvecOmegaYmax(1))),'ko-')
hold on
h2=semilogy(study.omegaVec/10,u/max(u(1)),'k--x')
set(gca,'Fontsize',17)
%plot(comsol(:,1),comsol(:,2)/(opt.Ly/2));
txt1 = ['$\leftarrow ' num2str(round(opt.phi(end)/5.44,3)) '\, V_{PI}$'];
%txt1 = ['$\leftarrow ' num2str(phi(end)/) '\, V_{PI}$'];
[umax,index]= max(abs(u)/max(abs(u(1))));


text(study.omegaVec(index)/10,umax,txt1,'interpreter','Latex','Fontsize',14)
xlabel('$\frac{\omega}{\omega_0}$','interpreter','Latex','Fontsize',25)
ylabel('$\frac{\delta x}{x_{init}}$','interpreter','Latex','Fontsize',25)
grid on
h=legend([h2,h1],{'Analytical solution','Monolothic formulation - \textbf{u}/p'},'location','southwest');
set(h,'Interpreter','latex');
sti='/Users/Anders/Dropbox/DTU/11.Speciale/Thesis/Sections/FEM/Verification/';
%h=legend('Analytical solution','Electromechanical-Acoustic system','location','northwest')
%set(h,'Interpreter','latex');
%sti='/Users/Anders/Dropbox/DTU/11.Speciale/Thesis/Sections/FEM/Verification/';
%pdfPrint('MonolitForceVeri1D',sti)