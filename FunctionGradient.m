function  [c,g,dc,dg,opt]=FunctionGradient(opt,mesh,study,flag)
    
    if mod(opt.itr,5)==1 && opt.itr >= 10 %&& opt.topopt.p<=15 %&& strcmp(opt.Projection,'yes')
        %opt.topopt.penal = opt.topopt.penal+1;
        %beta = beta*2;
        %display(['beta = ' num2str(beta)])
        opt.mmaItr = 1;
        %if itr > 10
        %opt.topopt.n = opt.topopt.n+1;
        %end
        if opt.topopt.p< 15
        opt.topopt.p = opt.topopt.p +1;
        end
    end
    
    %% Density filter and projections
    opt.topopt.xold = opt.topopt.x;
    opt.topopt.xFilt = opt.topopt.x;
    if strcmp(opt.Filt,'dens')
        opt.topopt.xFilt(opt.topopt.indActive) = (opt.topopt.H*opt.topopt.x(opt.topopt.indActive))./opt.topopt.Hs;
    end
    
    if strcmp(opt.Projection,'yes')
        [opt.topopt.xPhys,dx] = robustProj(opt.topopt.x,0.0,beta);
    else
        opt.topopt.xPhys = opt.topopt.xFilt;
    end
    
    %% Interpolation
    opt = intpol(opt,opt.intpmethod);
    
    %% FE ANALYSIS & sensitivities
    opt = Controller_Nonlinear(mesh,study,opt);
    study.omega = study.omegaVec(1);
%break
    %[cV,c0,dcV,dc0,cComp,dcComp] = csens_PressureAdd(opt,mesh,study,1);
    %[c,dc] = csens_Pressure(opt,mesh,study,1);
    [c,dc] = csens_PressureComplx02(opt,mesh,study,flag);
    %[c,dc] = csens_PressureComplx(opt,mesh,study,1);
    %c = -(cV-c0);
    %dc = -(dcV-dc0);
    %[c,dc] = csens_Ey(opt,mesh,study,1);
    
    if strcmp(opt.Filt,'sens')
        dc(opt.topopt.indActive) = (opt.topopt.H*(opt.topopt.x(opt.topopt.indActive).*dc(opt.topopt.indActive)))...
            ./(max(opt.topopt.x(opt.topopt.indActive),1e-3).*opt.topopt.Hs);
    elseif strcmp(opt.Filt,'dens') && ~strcmp(opt.Projection,'yes')
        dc(opt.topopt.indActive) = opt.topopt.H*(dc(opt.topopt.indActive)./opt.topopt.Hs);
    elseif strcmp(opt.Filt,'dens') && strcmp(opt.Projection,'yes')
        dc(opt.topopt.indActive) = opt.topopt.H*(dc(opt.topopt.indActive).*dx(opt.topopt.indActive)./opt.topopt.Hs);
    end
    
    % Store objective function for plotting
    
    opt.topopt.xmat(:,opt.itr) = opt.topopt.xPhys;
    % Scaling of objective function and sensitivities
    if opt.itr == 1
        opt.topopt.cScale = 1;%10/abs(c);
        
        %cc0 = -opt.topopt.cScale*c*100;
        %cc0 = cc0^2;
    end
 %   cComp = cComp*opt.topopt.ccScale;
  %  dcComp = dcComp*opt.topopt.ccScale;
   % dcComp = cComp*2*dcComp;
    %cComp = cComp^2;
    dc = -1/c/log(10)*dc;
    c = -log10(c*opt.topopt.cScale);
    disp(['OBJ: ' num2str(c)])

    %cc = cc*opt.topopt.ccScale;
    % for FD check
    %dc = -dc*opt.topopt.cScale;
    %cdc = cdc*opt.topopt.ccScale;
    opt.topopt.objective(opt.itr)=(c);
    %dc0 = dc;
    
    
    %c = log10(c);
    %dc = 2*c*dc;
    %c = c^2;
    %dc = 2*dc/(c*log(10));
    %c = log10(c^2);
    
    
    %c0 = c;
    %dc0= dc;
    
    %% Constraint functions and derivatives
    
    Mnd = sum(4*opt.topopt.xPhys(opt.topopt.indActive).*(1-opt.topopt.xPhys(opt.topopt.indActive))/length(opt.topopt.indActive))*100;
    %if  Mnd0 >=0.1 && Mnd-Mnd0<0 && mod(opt.itr,15)==1 %&& opt.itr>70 &&  Mnd <= 15
    %    Mnd0 = Mnd0 - 0.1* Mnd0;
    %    mmaopt.itr = 1;
    %end
    g = sum(opt.topopt.xPhys(opt.topopt.indActive))./(opt.topopt.volfrac*numel(opt.topopt.indActive))-1;
    %g(2,1) = cc0-c; 
    dg = ones(1,numel(opt.topopt.indActive))./(opt.topopt.volfrac*numel(opt.topopt.indActive));
    if strcmp(opt.Filt,'dens') && ~strcmp(opt.Projection,'yes')
        dg = (opt.topopt.H*(dg'./opt.topopt.Hs))';
    end
    if strcmp(opt.Filt,'dens') && strcmp(opt.Projection,'yes')
       dg = (opt.topopt.H*(dg'.*dx(opt.topopt.indActive)./opt.topopt.Hs))';
    end
    %dg(2,:) = dcComp(opt.topopt.indActive)';%(4*ones(1,numel(opt.topopt.indActive))-8*opt.topopt.xPhys(opt.topopt.indActive)')/length(opt.topopt.indActive)*100; %cdc(opt.topopt.indActive);
    
    opt.topopt.vol(opt.itr) = sum(opt.topopt.xPhys(opt.topopt.indActive))./(numel(opt.topopt.indActive)); % store vol constraint
%    opt.topopt.pcon(opt.itr) = cComp;
    opt.topopt.Mnd(opt.itr) = Mnd;
    % Printing
    
    %% Vol con turned around
    g = opt.VolConDir*g;
    dg = opt.VolConDir*dg;
    
    %% FDcheck
    if strcmp(opt.topopt.FDcheck,'yes') %&& opt.itr==1
        FDcheck(mesh,study,opt,opt.topopt.x,c0,dc0,g,dg,intpmethod,FDelement)
    end
    
    if opt.itr ==1
        mkdir([opt.projectname]);
    end
    
    %% Para view writing for every nth iteration
    if mod(opt.itr,opt.nthplot)==1 && strcmp(opt.itPlot,'yes')
        %opt.topopt.dc = dc;
        %mkdir([projectname])
        save([opt.projectname '/' 'opt.mat'],'opt')
        %save(['PLOT/' projectname '/' 'objective_' num2str(itr) '.mat'],'opt.topopt.objective')
        %save(['PLOT/' projectname '/' 'vol_' num2str(itr) '.mat'],'opt.topopt.vol')
        %ParaWrite(opt,study,mesh,projectname)
    end


end
