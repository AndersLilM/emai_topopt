clear
close all


%% addpath to FEA, MESH and VISUALIZATION (ParaView)
addpath('FEM')
addpath('MESH')
addpath('PLOT')
% addpath('TopOpt/MMA')
addpath('GCMMA')
addpath('TopOpt')


%% Material properties
opt.plotfields = 1;
% Properties of the solid material
Es= 2e9 / 1e5;
rhos = 1000 / 1e5;
opt.topopt.nu=0.4;
opt.topopt.thk=1;

Ks = Es/(2*(1-opt.topopt.nu));                      % Bulk modulus 2D plane stress
%Ks = Es/(2*(1+opt.topopt.nu)*(1-2*opt.topopt.nu)); % Bulk modulus 2D plane strain
Gs = Es/(2*(1+opt.topopt.nu));                      % Shear modulus

%Properties of acoustic material
Ka = 1.42e5 /1e5; %1.42e5*1e-10;%28.5e9;%1e-10%
rhoa = 1.293 /1e5 ;%1e-4;%1.293*1e-10;%1.293;
Ga = 1e-10;%Gs*1e-12;%0.000001;
% Electric properties
eps0 = 8.85e-12;%*1e5;
epsS = eps0;
epsG = 10^5*eps0;
epsA = eps0;

%% FE Analysis
opt.nGauss = 'reduced'  % Integration of electric forces and field
opt.Fflag = 'on';
opt.LossCoeff = 1e-3;

% Applied voltage and forcing frequency

opt.phi = str2double(getenv('phi'));%*[0.046]%0.04;%[0.01:0.05:0.21,0.25];%[0.01,0.41,0.45,,0.5,0.51];%[0];
%opt.phi = 0.0025;
study.omegaVec = str2double(getenv('omega'));%1e6;%1.1e7; %9e6%1.5e7;%[1:2:100]*1e4;%[1]*1e5;
%study.omegaVec = 0.1e6;
opt.VolConDir = str2double(getenv('VolConDir'));
opt.VolConDir = -1;
%opt.obj = 'acoustic';
% Material vector used for RAMP^
opt.topopt.Kvec = [Ks Ka];
opt.topopt.Gvec = [Gs Ga];
opt.topopt.rhovec = [rhos rhoa];
opt.topopt.permvecg = [epsG epsA];
opt.topopt.permvecr = [epsS eps0];

opt.topopt.volfrac = 0.4;

%% Support & Mesh
opt.Lx=200e-6;
opt.Ly=200e-6;
opt.nelx=100;
opt.nely=100;
opt.ne = opt.nelx*opt.nely;
support='New';
opt.obj = 'acoustic'; %% Chose objective dofs 'disp' / 'acoustic'
study.ZeroAnalysis = 1;


%[mesh.Q9, opt] = MBB2_StructMeshGenerator_Q9(Lx,Ly,nelx,nely,0,0,0,opt.topopt.thk,support,opt);
[mesh, opt] =NewProb3Supp_ElectricMeshGenerator_Q9(opt.Lx,opt.Ly,opt.nelx,opt.nely,0,0,0,opt.topopt.thk,support,opt);
opt.U(:,1) = zeros(mesh.Q9.neqn+2*mesh.Q4.neqn,1);
%mesh = MixedMesh_Q9toQ4(mesh);




%% initilization TopOpt
%opt.topopt.volfrac = 0.5;       % Volume fraction
%opt.topopt.x(1:ne,1) = 1;       % Passive elements =1
opt.topopt.x(opt.topopt.indActive) = opt.topopt.volfrac; %Volume fraction of active elements
%opt.topopt.x(opt.topopt.indActive) = rand(length(opt.topopt.x(opt.topopt.indActive)),1);
%load('xdensAcou08');
  %load('PLOT/Resultater/4975e3rads_vol02_sg00_2RsqrSens_minMidObj/xdens');

%opt.topopt.x(opt.topopt.indActive) = xdensAcou08(opt.topopt.indActive);
%% interpolation parameters
opt.intpmethod = 'RAMP';             % RAMP/SIMP
opt.topopt.n = 12;            % Penalty for RAMP
opt.topopt.p = 3;%201;             % penalty Simp

%% Optmization parameters
opt.topopt.optmzer = 'MMA';     % MMA/OC
opt.topopt.movelimit = 0.2;     % Move limit for MMA/OC
opt.topopt.FDcheck = 'no';     % Runs FD check for first iteration
FDelement = opt.topopt.indActive([1]); %Elements subjected to FD check
opt.BCcheck = 'no';             % Writes BC to paraview file
opt.Filt = getenv('filter')%'sens'; %  filter sens/dens/no
opt.Filt = 'dens';
%% Paraview iteration plotting options
opt.itPlot = 'yes'
opt.nthplot = 10;   % writes paraview file for every nth iteration
%projectname = 'ProjectionTest_StringOpen_volfrac_02_v0046_R1sqr' % Subname for paraview file

opt.itr = 0
opt.mmaItr = 0; 
change = 1;
opt.Projection = 'no'
%% Filter
opt.topopt.R = str2double(getenv('R'))*sqrt(2*(opt.Lx/opt.nelx)^2); %% Radius defined from element size

%opt.topopt.R = 1*sqrt(2*(opt.Lx/opt.nelx)^2); %% Radius defined from element size

if strcmp(opt.Filt,'sens')
    opt = SetupSensFilter(opt,mesh);
elseif strcmp(opt.Filt,'dens')
    opt = SetupFilter(opt,mesh);
end
opt.projectname = ['NewObj_Absorb_Vol' num2str(opt.topopt.volfrac) '_V' num2str(opt.phi) '_' num2str(study.omegaVec/1e6) 'e6' '_' opt.Filt 'R' '_Volcon' num2str(opt.VolConDir)];
beta = 1;
Mnd = sum(4*opt.topopt.x(opt.topopt.indActive).*(1-opt.topopt.x(opt.topopt.indActive))/length(opt.topopt.indActive))*100;
Mnd0 = Mnd;
% % 
%xplot = reshape(opt.topopt.x,opt.nely,opt.nelx);
 %colormap(gray);
% %      handle = subplot(2,2,1);
%h = imagesc(-[real(xplot)]); axis equal; axis tight;set(gca,'YDir','normal');pause(1e-30);colorbar;
% %      saveas(h,'PNGDumpFromBatchScript.png');
% %      save('DataDumpFromBatchScript.mat','opt');
 %      save('DataDumpFromBatchScript.mat','opt');

opt     
     
%% HERE COMES THE GCMMA PART
% 2 function needed - returns f and g, returns f g df fg

m = 1;
n = length(opt.topopt.indActive);
epsimin = sqrt(m+n)*10^(-9);
eeen    = ones(n,1);
eeem    = ones(m,1);
zeron   = zeros(n,1);
zerom   = zeros(m,1);
xval    = opt.topopt.x(opt.topopt.indActive); %5*eeen;
xold1   = xval;
xold2   = xval;
xmin    = 0.00001*eeen;
xmax    = eeen;
low     = xmin;
upp     = xmax;
c       = 1000*eeem;
d       = eeem;
a0      = 1;
a       = zerom;
raa0    = 0.01;
raa     = 0.01*eeem;
raa0eps = 0.00001;
raaeps  = 0.00001*eeem;
outeriter = 0;
maxitte  = 200;
kkttol  = 0.00001;
%

opt.itr = opt.itr +1;
opt.mmaItr = opt.mmaItr + 1;
    
if outeriter < 0.5
    %[f0val,df0dx,fval,dfdx] = beam2(xval);
    % MY FUNCTION
    [f0val,fval,df0dx,dfdx,opt] = FunctionGradient(opt,mesh,study,1);
    innerit=0;
    outvector = [outeriter innerit f0val fval' xval']
end

opt.itr = opt.itr +1;
opt.mmaItr = opt.mmaItr + 1;

%%%% The outer iterations start:
kktnorm = kkttol+10;
outitte = 0;
while kktnorm > kkttol & outitte < maxitte
    outitte   = outitte+1;
    outeriter = outeriter+1;
    %%%% The parameters low, upp, raa0 and raa are calculated:
    [low,upp,raa0,raa] = ...
        asymp(outeriter,eeen,xval,xold1,xold2,xmin,xmax,low,upp, ...
        raa0,raa,raa0eps,raaeps);
    
    %%%% The GCMMA subproblem is solved at the point xval:
    [xmma,ymma,zmma,lam,xsi,eta,mu,zet,s,f0app,fapp] = ...
        gcmmasub(m,n,outeriter,epsimin,xval,xmin,xmax,low,upp, ...
        raa0,raa,f0val,df0dx(opt.topopt.indActive),fval,dfdx,a0,a,c,d);
    %%%% The user should now calculate function values (no gradients)
    %%%% of the objective- and constraint functions at the point xmma
    %%%% ( = the optimal solution of the subproblem).
    %%%% The results should be put in f0valnew and fvalnew.
    
%     [f0valnew,fvalnew] = beam1(xmma);
    % MY FUNCTION
    opt.topopt.x(opt.topopt.indActive) = xmma;
    [f0valnew,fvalnew,~,~,opt] = FunctionGradient(opt,mesh,study,0);
    
    %%%% It is checked if the approximations are conservative:
    [conserv] = concheck(m,epsimin,f0app,f0valnew,fapp,fvalnew);
    %%%% While the approximations are non-conservative (conserv=0),
    %%%% repeated inner iterations are made:
    innerit=0;
    if conserv == 0
        while conserv == 0 & innerit <= 10
            innerit = innerit+1;
            fprintf('inner: %i\n',innerit)
            %%%% New values on the parameters raa0 and raa are calculated:
            [raa0,raa] = ...
                raaupdate(xmma,xval,xmin,xmax,low,upp,f0valnew,fvalnew, ...
                f0app,fapp,raa0,raa,raa0eps,raaeps,epsimin);
            %%%% The GCMMA subproblem is solved with these new raa0 and raa:
            [xmma,ymma,zmma,lam,xsi,eta,mu,zet,s,f0app,fapp] = ...
                gcmmasub(m,n,outeriter,epsimin,xval,xmin,xmax,low,upp, ...
                raa0,raa,f0val,df0dx(opt.topopt.indActive),fval,dfdx,a0,a,c,d);
            %%%% The user should now calculate function values (no gradients)
            %%%% of the objective- and constraint functions at the point xmma
            %%%% ( = the optimal solution of the subproblem).
            %%%% The results should be put in f0valnew and fvalnew:
        %    [f0valnew,fvalnew] = beam1(xmma);
            opt.topopt.x(opt.topopt.indActive) = xmma;

            [f0valnew,fvalnew,~,~,opt] = FunctionGradient(opt,mesh,study,0);
            disp(['c: ' num2str(f0valnew)])
				%%%% It is checked if the approximations have become conservative:
            [conserv] = concheck(m,epsimin,f0app,f0valnew,fapp,fvalnew);
        end
    end
    %%%% No more inner iterations. Some vectors are updated:
    xold2 = xold1;
    xold1 = xval;
    xval  = xmma;
    %%%% The user should now calculate function values and gradients
    %%%% of the objective- and constraint functions at xval.
    %%%% The results should be put in f0val, df0dx, fval and dfdx:
    % [f0val,df0dx,fval,dfdx] = beam2(xval);
        opt.topopt.x(opt.topopt.indActive) = xmma;
        
        opt.itr = opt.itr +1;
        opt.mmaItr = opt.mmaItr + 1;
        
    [f0val,fval,df0dx,dfdx,opt] = FunctionGradient(opt,mesh,study,1);
        %%%% The residual vector of the KKT conditions is calculated:
    [residu,kktnorm,residumax] = ...
        kktcheck(m,n,xmma,ymma,zmma,lam,xsi,eta,mu,zet,s, ...
        xmin,xmax,df0dx(opt.topopt.indActive),fval,dfdx,a0,a,c,d);
    outvector = [outeriter innerit f0val fval' xval' kktnorm];
    change = max(abs(xval-xold1));
	 disp(['Outer.It.: ' sprintf('%4i',outeriter) ' Inner.It.: ' sprintf('%4i',innerit) ' Obj.: ' sprintf('%10.4f',f0val) ...
      ' Con.: ' sprintf('%6.3f',fval(1)) ...
      ' ch.: ' sprintf('%6.3f',change )])
end

    %% Para view writing for every nth iteration
    if mod(opt.itr,opt.nthplot)==1 && strcmp(itPlot,'yes')
        %opt.topopt.dc = dc;
        %mkdir([projectname])
        save([opt.projectname '/' 'opt.mat'],'opt')
        %save(['PLOT/' projectname '/' 'objective_' num2str(itr) '.mat'],'opt.topopt.objective')
        %save(['PLOT/' projectname '/' 'vol_' num2str(itr) '.mat'],'opt.topopt.vol')
        %ParaWrite(opt,study,mesh,projectname)
    end
    toc
    
    exit
%   
% 
% 
% 
% 
% 
% 
% 
% 
% 
% %% OLD CODE
% 
% while  itr <=2000 %change > 0.00001 &&
%     tic
%     itr = itr +1;
%     mmaItr = mmaItr + 1;
%     if mod(itr,5)==1 && itr >= 10 %&& opt.topopt.p<=15 %&& strcmp(opt.Projection,'yes')
%         %opt.topopt.penal = opt.topopt.penal+1;
%         %beta = beta*2;
%         %display(['beta = ' num2str(beta)])
%         mmaItr = 1;
%         %if itr > 10
%         %opt.topopt.n = opt.topopt.n+1;
%         %end
%         if opt.topopt.p< 15
%         opt.topopt.p = opt.topopt.p +1;
%         end
%     end
%     
%     %% Density filter and projections
%     opt.topopt.xold = opt.topopt.x;
%     opt.topopt.xFilt = opt.topopt.x;
%     if strcmp(opt.Filt,'dens')
%         opt.topopt.xFilt(opt.topopt.indActive) = (opt.topopt.H*opt.topopt.x(opt.topopt.indActive))./opt.topopt.Hs;
%     end
%     
%     if strcmp(opt.Projection,'yes')
%         [opt.topopt.xPhys,dx] = robustProj(opt.topopt.x,0.0,beta);
%     else
%         opt.topopt.xPhys = opt.topopt.xFilt;
%     end
%     
%     %% Interpolation
%     opt = intpol(opt,opt.intpmethod);
%     
%     %% FE ANALYSIS & sensitivities
%     opt = Controller_Nonlinear(mesh,study,opt);
%     study.omega = study.omegaVec(1);
% %break
%     %[cV,c0,dcV,dc0,cComp,dcComp] = csens_PressureAdd(opt,mesh,study,1);
%     %[c,dc] = csens_Pressure(opt,mesh,study,1);
%     [c,dc] = csens_PressureComplx02(opt,mesh,study,1);
%     %[c,dc] = csens_PressureComplx(opt,mesh,study,1);
%     %c = -(cV-c0);
%     %dc = -(dcV-dc0);
%     %[c,dc] = csens_Ey(opt,mesh,study,1);
%     
%     if strcmp(opt.Filt,'sens')
%         dc(opt.topopt.indActive) = (opt.topopt.H*(opt.topopt.x(opt.topopt.indActive).*dc(opt.topopt.indActive)))...
%             ./(max(opt.topopt.x(opt.topopt.indActive),1e-3).*opt.topopt.Hs);
%     elseif strcmp(opt.Filt,'dens') && ~strcmp(opt.Projection,'yes')
%         dc(opt.topopt.indActive) = opt.topopt.H*(dc(opt.topopt.indActive)./opt.topopt.Hs);
%     elseif strcmp(opt.Filt,'dens') && strcmp(opt.Projection,'yes')
%         dc(opt.topopt.indActive) = opt.topopt.H*(dc(opt.topopt.indActive).*dx(opt.topopt.indActive)./opt.topopt.Hs);
%     end
%     
%     % Store objective function for plotting
%     
%     opt.topopt.xmat(:,itr) = opt.topopt.xPhys;
%     % Scaling of objective function and sensitivities
%     if itr == 1
%         opt.topopt.cScale = 10/abs(c);
%         
%         %cc0 = -opt.topopt.cScale*c*100;
%         %cc0 = cc0^2;
%     end
%  %   cComp = cComp*opt.topopt.ccScale;
%   %  dcComp = dcComp*opt.topopt.ccScale;
%    % dcComp = cComp*2*dcComp;
%     %cComp = cComp^2;
%     dc = -1/c/log(10)*dc;
%     c = -log10(c*opt.topopt.cScale);
%     %cc = cc*opt.topopt.ccScale;
%     % for FD check
%     %dc = -dc*opt.topopt.cScale;
%     %cdc = cdc*opt.topopt.ccScale;
%     opt.topopt.objective(itr)=(c);
%     %dc0 = dc;
%     
%     
%     %c = log10(c);
%     %dc = 2*c*dc;
%     %c = c^2;
%     %dc = 2*dc/(c*log(10));
%     %c = log10(c^2);
%     
%     
%     %c0 = c;
%     %dc0= dc;
%     
%     %% Constraint functions and derivatives
%     
%     Mnd = sum(4*opt.topopt.xPhys(opt.topopt.indActive).*(1-opt.topopt.xPhys(opt.topopt.indActive))/length(opt.topopt.indActive))*100;
%     %if  Mnd0 >=0.1 && Mnd-Mnd0<0 && mod(itr,15)==1 %&& itr>70 &&  Mnd <= 15
%     %    Mnd0 = Mnd0 - 0.1* Mnd0;
%     %    mmaItr = 1;
%     %end
%     g = sum(opt.topopt.xPhys(opt.topopt.indActive))./(opt.topopt.volfrac*numel(opt.topopt.indActive))-1;
%     %g(2,1) = cc0-c; 
%     dg = ones(1,numel(opt.topopt.indActive))./(opt.topopt.volfrac*numel(opt.topopt.indActive));
%     if strcmp(opt.Filt,'dens') && ~strcmp(opt.Projection,'yes')
%         dg = (opt.topopt.H*(dg'./opt.topopt.Hs))';
%     end
%     if strcmp(opt.Filt,'dens') && strcmp(opt.Projection,'yes')
%        dg = (opt.topopt.H*(dg'.*dx(opt.topopt.indActive)./opt.topopt.Hs))';
%     end
%     %dg(2,:) = dcComp(opt.topopt.indActive)';%(4*ones(1,numel(opt.topopt.indActive))-8*opt.topopt.xPhys(opt.topopt.indActive)')/length(opt.topopt.indActive)*100; %cdc(opt.topopt.indActive);
%     
%     opt.topopt.vol(itr) = sum(opt.topopt.xPhys(opt.topopt.indActive))./(numel(opt.topopt.indActive)); % store vol constraint
% %    opt.topopt.pcon(itr) = cComp;
%     opt.topopt.Mnd(itr) = Mnd;
%     % Printing
%     
%     %% Vol con turned around
%     g = opt.VolConDir*g;
%     dg = opt.VolConDir*dg;
%     
%     %% FDcheck
%     if strcmp(opt.topopt.FDcheck,'yes') %&& itr==1
%         FDcheck(mesh,study,opt,opt.topopt.x,c0,dc0,g,dg,opt.intpmethod,FDelement)
%     end
%     %% Optimizer
%     if strcmp(opt.topopt.optmzer,'MMA')
%         [xnew,opt] = MMA(opt,mmaItr,c,dc(opt.topopt.indActive),g,dg);
%     elseif strcmp(opt.topopt.optmzer,'OC')
%         [xnew] = OC(opt,opt.topopt.x(opt.topopt.indActive),dc(opt.topopt.indActive),g,dg');
%     end
%     opt.topopt.x(opt.topopt.indActive) = xnew;
%     if itr>200
%     change = max(abs(opt.topopt.x(opt.topopt.indActive)-opt.topopt.xold(opt.topopt.indActive)));
%     end
%     disp([' It.: ' sprintf('%4i',itr) ' Obj.: ' sprintf('%10.4f',c) ...
%         ' Vol.: ' sprintf('%6.3f',g(1,1)) ...
%         ' pcon.: ' sprintf('%6.3f',0) ' ch.: ' sprintf('%6.3f',change )])
%     %% Plotting
%     h3 = figure(3);
%     ssize=get( groot, 'Screensize' );
%     set(h3,'position',[0, 0, ssize(3), ssize(4)])
%     %h3('position', [0, 0, ssize(3), ssize(4)]);
%      
%     xplot = reshape(opt.topopt.xPhys,opt.nely,opt.nelx);
%     colormap(gray);
%     handle = subplot(2,2,1);
%     h = imagesc(-[real(xplot)]); axis equal; axis tight;set(gca,'YDir','normal');pause(1e-30);colorbar;
%     subplot(2,2,2)
%     plot(opt.topopt.objective)
%     xlabel('iteration','Fontsize',16)
%     ylabel('objective','Fontsize',16)
%     title(projectname)
%     subh1 = subplot(2,2,3);
%     %plot(g(2),'k')
%     hold on
%     %plot([1,itr],[cc0,cc0],'r--')
%     %plot([1,itr],[opt.topopt.volfrac,opt.topopt.volfrac],'r--')
%    %set(subh1,'Ylim',[0 1])
%     xlabel('iteration','Fontsize',16)
%     ylabel('obj. con.','Fontsize',16)
%     subplot(2,2,4)
%     plot(opt.topopt.Mnd,'k');
%     hold on
%    % plot([1,itr],[cc0/opt.topopt.ccScale^2,cc0/opt.topopt.ccScale^2],'r--')
%     plot([1,itr],[Mnd0,Mnd0],'r--');
%     xlabel('iteration','Fontsize',16)
%     ylabel('Mnd [%]','Fontsize',16)
%     %saveas(h3,'PNGDumpFromScript.png');
%     if itr ==1
%     mkdir([projectname]);
%     end
%     
%     saveas(h3,'Objective.png');
%     saveas(figure(2),[projectname '/' 'it_' num2str(itr) '_Potential.png']);
%     saveas(figure(1),[projectname '/' 'it_' num2str(itr) '_PressureDisp.png']);
% 
%     
%     
%     %% Para view writing for every nth iteration
%     if mod(itr,nthplot)==1 && strcmp(itPlot,'yes')
%         %opt.topopt.dc = dc;
%         %mkdir([projectname])
%         save([projectname '/' 'opt.mat'],'opt')
%         %save(['PLOT/' projectname '/' 'objective_' num2str(itr) '.mat'],'opt.topopt.objective')
%         %save(['PLOT/' projectname '/' 'vol_' num2str(itr) '.mat'],'opt.topopt.vol')
%         %ParaWrite(opt,study,mesh,projectname)
%     end
%     toc
% end
% 
% 
% exit
% 
% 
