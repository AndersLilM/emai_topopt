function PlotFields(mesh,param,u,v,phi,Tx,Ty,p,pobj,color)
% plot the solution
%
% mesh:     the mesh struct
% param:    parameters for visualization
%           .title = 'string'
%           .name = 'string'
for sol=1:1;
    
    % Create the patch
    nel = size(mesh.IX,1);
    xdata = zeros(nel,4);
    ydata = zeros(nel,4);
    udata = zeros(nel,4);
    vdata = zeros(nel,4);
    wdata = zeros(nel,4);
    pdata = zeros(nel,4);
    pobjdata = zeros(nel,4);
    Txdata = zeros(nel,4);
    Tydata = zeros(nel,4);
    for e=1:nel
        nen =  mesh.IX(e,2:5);
        xy = mesh.X(nen,2:3);
        xdata(e,:) = xy(:,1);
        ydata(e,:) = xy(:,2);
        %for i=1:4
        %    edof(2*i-1) = 2*nen(i)-1;
        %    edof(2*i-0) = 2*nen(i)-0;
        %end
        udata(e,:)=u(nen);
        vdata(e,:)=v(nen);
        wdata(e,:)=phi(nen);
        pdata(e,:)=p(nen);
        pobjdata(e,:)=pobj(nen);
        Txdata(e,:) = Tx(nen);
        Tydata(e,:) = Ty(nen);
    end
    ssize=get( groot, 'Screensize' );
    h1=figure('position', [0, 0, ssize(3), ssize(4)]);
    subplot(1,4,1)
    patch(xdata',ydata',real(udata)','edgecolor',color)
    if ~isempty(param)
        % check if title is there
        title(param.title)
    else 
        title('u [m]')
    end
    axis equal
    colormap jet
    colorbar
    
    subplot(1,4,2)
    patch(xdata',ydata',real(vdata)','edgecolor',color)
    title(['v [m]'])
    axis equal
    colormap jet
    colorbar
    
     subplot(1,4,3)
    patch(xdata',ydata',real(pdata)','edgecolor',color)
    title(['(p0-p)^2 [Pa]'])
    axis equal
    colormap jet
    colorbar
    
    subplot(1,4,4)
    patch(xdata',ydata',real(pobjdata)','edgecolor','k')
    title(['obj.domain'])
    axis equal
    colormap jet
    %colorbar
    
    
    
    h2=figure('position', [0, 0, ssize(3), ssize(4)]);
    subplot(1,4,1)
    patch(xdata',ydata',real(wdata)','edgecolor',color)
    title(['\phi [V]'])
    axis equal
    colormap jet
    colorbar
    
    subplot(1,4,2)
    patch(xdata',ydata',real(Txdata)','edgecolor',color)
    title(['Tx [N]'])
    axis equal
    colormap jet
    colorbar
    
    subplot(1,4,3)
    patch(xdata',ydata',real(Tydata)','edgecolor',color)
    title(['Ty [N]'])
    axis equal
    colormap jet
    colorbar
    
    subplot(1,4,4)
    patch(xdata',ydata',real(pobjdata)','edgecolor','k')
    title(['Obj.domain'])
    axis equal
    colormap jet
    %colorbar
    
    
    
end

end