function [] = PlotVars(opt,mesh)
% Defines the variables to be plotted by the PlotFields function

% Find structural dofs
jeps = find(opt.topopt.x>0);
structDofsQ4 = mesh.Q4.IX(jeps,2:5);
structDofsQ4 = unique(reshape(structDofsQ4,numel(structDofsQ4),1));

jeps = find(opt.topopt.x==0);
structDofsQ9 = mesh.Q9.IX(jeps,2:10);
structDofsQ9 = unique(reshape(structDofsQ9,numel(structDofsQ9),1));

if exist('opt.DU0')
U = (opt.DU0-opt.DU).*conj(opt.DU0-opt.DU); % Substitute with opt.U to plot the static deflection
elseif sum(opt.DU>0) 
     U = opt.DU;
else
    U = opt.U;
end

Uobj =opt.DU*0; % plot objective domain
if exist('opt.EDOFs')
Uobj(opt.EDOFs) = 1;
end
%% Defining input for plot routine

    u = U(mesh.Q4.neqn+(1:2:mesh.Q9.neqn));
    u(structDofsQ9) = 0;
    v = U(mesh.Q4.neqn+(2:2:mesh.Q9.neqn));
    v(structDofsQ9) = 0;
    e = opt.U((mesh.Q4.neqn+mesh.Q9.neqn+1):end);
    p = U(1:mesh.Q4.neqn);
    pobj = Uobj(1:mesh.Q4.neqn);
    p = 20*log10(abs(p)/(20e-6)); %% sound pressure level in dB 
     p(structDofsQ4) = 0;
    Tx = opt.P(mesh.Q4.neqn+(1:2:mesh.Q9.neqn));
    Ty = opt.P(mesh.Q4.neqn+(2:2:mesh.Q9.neqn));
    PlotFields(mesh.Q4,[],u(mesh.Q4nodeIndex),v(mesh.Q4nodeIndex),e,Tx(mesh.Q4nodeIndex),Ty(mesh.Q4nodeIndex),p,pobj,'none')
    

end

