% Plotting routine for density distribution postproc

sti='/Users/Anders/Dropbox/DTU/12.ResearchAssistant/Results/Results/';
close all
for i = 1:length(opt.topopt.xmat(1,:))
    
   % figure(1)
xplot = reshape(opt.topopt.xmat(:,i),opt.nely,opt.nelx);
    colormap(gray);
  subplot(2,1,1);
    [AX] = imagesc(-[real(xplot)]); axis equal; axis tight;set(gca,'YDir','normal');pause(1);axis on;
    set(gca,'YTick',[]) % empty vector
%set(gca,'XTick',[])
    %pdfPrint(['xdens_' num2str(i)] ,sti)
% AX is a handle to both axes
% AX(1) is "first" plot and
% AX(2) is "second" plot

%figure(2)
  subplot(2,1,2);
    semilogy(opt.topopt.objective(1:i),'k-')
    
  %  set(gca,'Fontsize',17)
  %  xlabel('$Iteration$','interpreter','Latex','Fontsize',25)
%ylabel('$\Phi \,\, [\mathrm{Pa}^2\, \mathrm{m}^2]$','interpreter','Latex','Fontsize',25)
%pdfPrint(['Obj_' num2str(i)] ,sti)
end

