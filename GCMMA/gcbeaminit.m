%---------------------------------------------------------------------
%  This is the file gcbeaminit.m.  Version April 2007.
%  Written by Krister Svanberg <krille@math.kth.se>
%
m = 1;
n = 5;
epsimin = sqrt(m+n)*10^(-9);
eeen    = ones(n,1);
eeem    = ones(m,1);
zeron   = zeros(n,1);
zerom   = zeros(m,1);
xval    = 5*eeen;
xold1   = xval;
xold2   = xval;
xmin    = eeen;
xmax    = 10*eeen;
low     = xmin;
upp     = xmax;
c       = 1000*eeem;
d       = eeem;
a0      = 1;
a       = zerom;
raa0    = 0.01;
raa     = 0.01*eeem;
raa0eps = 0.00001;
raaeps  = 0.00001*eeem;
outeriter = 0;
maxitte  = 20;
kkttol  = 0.00001;
%
%---------------------------------------------------------------------
