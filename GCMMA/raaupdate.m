%    This is the file raaupdate.m
%
function [raa0,raa] = ...
raaupdate(xmma,xval,xmin,xmax,low,upp,f0valnew,fvalnew, ...
          f0app,fapp,raa0,raa,raa0eps,raaeps,epsimin);
%
%    written in August 1999 by
%    Krister Svanberg <krille@math.kth.se>
%    Department of Mathematics, KTH,
%    SE-10044 Stockholm, Sweden.
%
raacofmin = 10^(-13);
eem = ones(size(raa));
yyux = (xmma-xval)./(upp-xmma);
yyxl = (xmma-xval)./(xmma-low);
raacof = 0.5*yyux'*yyxl;
raacof = max(raacof,raacofmin);

f0appe = f0app+0.5*epsimin;
if f0valnew > f0appe
  deltaraa0 = (1/raacof)*(f0valnew-f0app);
  zz0 = 1.1*(raa0 + deltaraa0);
  zz0 = min(zz0,10*raa0);
  raa0 = zz0;
end

fappe = fapp+0.5*epsimin*eem;
fdelta = fvalnew-fappe;
deltaraa = (1/raacof)*(fvalnew-fapp);
zzz = 1.1*(raa + deltaraa);
zzz = min(zzz,10*raa);
raa(find(fdelta > 0)) = zzz(find(fdelta > 0));



