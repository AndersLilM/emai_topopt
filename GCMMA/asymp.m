%    This is the file asymp.m
%
function [low,upp,raa0,raa] = ...
asymp(iter,eeen,xval,xold1,xold2,xmin,xmax,low,upp, ...
      raa0,raa,raa0eps,raaeps);
%
%    written in Aug 1999 by
%    Krister Svanberg <krille@math.kth.se>
%    Department of Mathematics, KTH,
%    SE-10044 Stockholm, Sweden.
%
if iter > 1.5
  raa0 = max(raa0eps,0.1*raa0);
  raa  = max(raaeps,0.1*raa);
end
if iter < 2.5
  low = xval - 0.5*(xmax-xmin);
  upp = xval + 0.5*(xmax-xmin);
else
  xxx = (xval-xold1).*(xold1-xold2);
  factor = eeen;
  factor(find(xxx > 0)) = 1.2;
  factor(find(xxx < 0)) = 0.7;
  low = xval - factor.*(xold1 - low);
  upp = xval + factor.*(upp - xold1);
  lowmin = xval - 10*(xmax-xmin);
  lowmax = xval - 0.01*(xmax-xmin);
  uppmin = xval + 0.01*(xmax-xmin);
  uppmax = xval + 10*(xmax-xmin);
  low = max(low,lowmin);
  low = min(low,lowmax);
  upp = min(upp,uppmax);
  upp = max(upp,uppmin);
end
