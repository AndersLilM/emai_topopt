%    This is the file gcmmasub.m
%
function [xmma,ymma,zmma,lam,xsi,eta,mu,zet,s,f0app,fapp] = ...
gcmmasub(m,n,iter,epsimin,xval,xmin,xmax,low,upp, ...
raa0,raa,f0val,df0dx,fval,dfdx,a0,a,c,d);
%
%    written in Aug 1999 by
%    Krister Svanberg <krille@math.kth.se>
%    Department of Mathematics, KTH,
%    SE-10044 Stockholm, Sweden.
%
eeen = ones(n,1);
zeron = zeros(n,1);

% Calculation of the bounds alfa and beta :
zzz = low + 0.1*(xval-low);
alfa = max(zzz,xmin);
zzz = upp - 0.1*(upp-xval);
beta = min(zzz,xmax);

% Calculations of p0, q0, r0, P, Q, r and b.
ux1 = upp-xval;
ux2 = ux1.*ux1;
xl1 = xval-low;
xl2 = xl1.*xl1;
ul1 = upp-low;
uxinv = eeen./ux1;
xlinv = eeen./xl1;
ulinv = eeen./ul1;
p0 = zeron;
p0(find(df0dx > 0)) = df0dx(find(df0dx > 0));
p0 = p0 + 0.5*raa0*ulinv;
p0 = p0.*ux2;
q0 = zeron;
q0(find(df0dx < 0)) = -df0dx(find(df0dx < 0));
q0 = q0 + 0.5*raa0*ulinv;
q0 = q0.*xl2;
r0 = f0val - p0'*uxinv - q0'*xlinv;
P = sparse(m,n);
P(find(dfdx > 0)) = dfdx(find(dfdx > 0));
P = P + 0.5*raa*ulinv';
P = P * spdiags(ux2,0,n,n);
Q = sparse(m,n);
Q(find(dfdx < 0)) = -dfdx(find(dfdx < 0));
Q = Q + 0.5*raa*ulinv';
Q = Q * spdiags(xl2,0,n,n);
r = fval - P*uxinv - Q*xlinv;
b = -r;

% Solving the subproblem by a primal-dual Newton method
[xmma,ymma,zmma,lam,xsi,eta,mu,zet,s] = ...
subsolv(m,n,epsimin,low,upp,alfa,beta,p0,q0,P,Q,a0,a,b,c,d);

% Calculations of f0app and fapp.
ux1 = upp-xmma;
xl1 = xmma-low;
uxinv = eeen./ux1;
xlinv = eeen./xl1;
f0app = r0 + p0'*uxinv + q0'*xlinv;
fapp  =  r +   P*uxinv +   Q*xlinv;

