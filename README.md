EMAI code repo:

Fun values for eksempel 2:

Phi: 0.0025 - 0.0050   begynder at bryder sammen ved phi = 0.05

omega: Første peak ligger omkring 0.5e6 rad/s
optimerer omkring 0.1e6 rad/s.
Har også set interassante designs ved 1.5e6 og 2.2 e6

Filter: Densitetsfilter laver mindst degenererede designs, men har det med at dø ved lavere spænding end sens filter.
Har brugt filter radius R = 1; svarende til én gange diagonalen af et element.

Variablen VolConDir ændrer fortegnen på volume constraint.

Alle andre variable skal p.t. ændres i Matlab scriptet

