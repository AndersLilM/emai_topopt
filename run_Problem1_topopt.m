clear
close all

%% addpath to FEA, MESH and VISUALIZATION (ParaView)
addpath('FEM')
addpath('MESH')
addpath('PLOT')
addpath('TopOpt/MMA')
addpath('TopOpt')


%% Material properties
opt.plotfields = 1;
% Properties of the solid material
Es=1e6;
rhos = 1;
opt.topopt.nu=0.17;
opt.topopt.thk=1;

Ks = Es/(2*(1-opt.topopt.nu));                      % Bulk modulus 2D plane stress
%Ks = Es/(2*(1+opt.topopt.nu)*(1-2*opt.topopt.nu)); % Bulk modulus 2D plane strain
Gs = Es/(2*(1+opt.topopt.nu));                      % Shear modulus

%Properties of acoustic material
Ka =0.6;%28.5e9;
rhoa = 1;%1.293;
Ga = 1e-10;%0.000001;
eps0 = 8.85e-12;
epsS = 1*eps0;
epsG = 10^5*eps0;
epsA = eps0;

%% FE Analysis
opt.nGauss = 'reduced'  % Integration of electric forces and field
opt.Fflag = 'on';
%study.analysis = 'static';
%study.SuppExc = 0;
%study.solvMeth = 'NR';
% Applied voltage and forcing frequency
opt.phi = str2double(getenv('phi'));%*[0.046]%0.04;%[0.01:0.05:0.21,0.25];%[0.01,0.41,0.45,,0.5,0.51];%[0];
study.omegaVec = str2double(getenv('omega'));
opt.obj = 'acoustic';
study.ZeroAnalysis = 0;
opt.LossCoeff = 0;
% Material vector used for RAMP^
opt.topopt.Kvec = [Ks Ka];
opt.topopt.Gvec = [Gs Ga];
opt.topopt.rhovec = [rhos rhoa];
opt.topopt.permvecg = [epsG epsA];
opt.topopt.permvecr = [epsS eps0];

opt.topopt.volfrac = str2double(getenv('volfrac'));
%% Support & Mesh
opt.Lx=6e-5;
opt.Ly=3e-5;
opt.nelx=120;%12*2;260;
opt.nely=60;%6*2;75;
opt.ne = opt.nelx*opt.nely;
%support='MBB';
support='GIL';
%[mesh.Q9, opt] = MBB2_StructMeshGenerator_Q9(Lx,Ly,nelx,nely,0,0,0,opt.topopt.thk,support,opt);
[mesh, opt] =stringOpen_ElectricMeshGenerator_Q9(opt.Lx,opt.Ly,opt.nelx,opt.nely,0,0,0,opt.topopt.thk,support,opt);
opt.U(:,1) = zeros(mesh.Q9.neqn+2*mesh.Q4.neqn,1);
%mesh = MixedMesh_Q9toQ4(mesh);




%% initilization TopOpt
%opt.topopt.volfrac = 0.5;       % Volume fraction
%opt.topopt.x(1:ne,1) = 1;       % Passive elements =1
opt.topopt.x(opt.topopt.indActive) = opt.topopt.volfrac; %Volume fraction of active elements

%% interpolation parameters
intpmethod = 'RAMP';             % RAMP/SIMP
opt.topopt.n = 6;            % Penalty for RAMP
opt.topopt.p = 3;             % penalty Simp

%% Optmization parameters
opt.topopt.optmzer = 'MMA';     % MMA/OC
opt.topopt.movelimit = 0.2;     % Move limit for MMA/OC
opt.topopt.FDcheck = 'no';     % Runs FD check for first iteration
FDelement = opt.topopt.indActive([1200]); %Elements subjected to FD check
opt.BCcheck = 'no';             % Writes BC to paraview file
opt.Filt = (getenv('filter'));%  filter sens/dens/no

%% Paraview iteration plotting options
itPlot = 'yes'
nthplot = 10;   % writes paraview file for every nth iteration
%projectname = 'ProjectionTest_StringOpen_volfrac_02_v0046_R1sqr' % Subname for paraview file
projectname = 'SIMPTest_dens_R2_n6p3Ram';
itr = 0;
mmaItr = 0;
change = 1;
opt.Projection = 'no'
%% Filter
opt.topopt.R = str2double(getenv('R'))*sqrt(2*(opt.Lx/opt.nelx)^2); % Radius defined from element size
if strcmp(opt.Filt,'sens')
    opt = SetupSensFilter(opt,mesh);
elseif strcmp(opt.Filt,'dens')
    opt = SetupFilter(opt,mesh);
end
beta = 0.1;
projectname = ['Problem1-volfrac' num2str(opt.topopt.volfrac) '-phi' num2str(opt.phi) '-freq' num2str(study.omegaVec/1e6) 'e6' '-' opt.Filt 'R' num2str(opt.topopt.R)];
while change > 0.001 && itr < 1000
    
    itr = itr +1;
    mmaItr = mmaItr + 1;
    if mod(itr,5)==1 && itr >= 5 %&& strcmp(opt.Projection,'yes')
        %opt.topopt.penal = opt.topopt.penal+1;
        %beta = beta+2;
        %display(['beta = ' num2str(beta)])
        %mmaItr = 1;
        %opt.topopt.penal = opt.topopt.penal+1;
        opt.topopt.p = opt.topopt.p +1;
        
    end
    
    %% Density filter and projections
    opt.topopt.xold = opt.topopt.x;
    opt.topopt.xFilt = opt.topopt.x;
    if strcmp(opt.Filt,'dens')
        opt.topopt.xFilt(opt.topopt.indActive) = (opt.topopt.H*opt.topopt.x(opt.topopt.indActive))./opt.topopt.Hs;
    end
    
    if strcmp(opt.Projection,'yes')
        [opt.topopt.xPhys,dx] = robustProj(opt.topopt.x,0.0,beta);
    else
        opt.topopt.xPhys = opt.topopt.xFilt;
    end
    
    %% Interpolation
    opt = intpol(opt,intpmethod);
    
    %% FE ANALYSIS & sensitivities
    opt = Controller_Nonlinear(mesh,study,opt);
    study.omega = study.omegaVec(1);
    
    [c,dc] = csens_Pressure(opt,mesh,study,1);
    
    if strcmp(opt.Filt,'sens')
        dc(opt.topopt.indActive) = (opt.topopt.H*(opt.topopt.x(opt.topopt.indActive).*dc(opt.topopt.indActive)))...
            ./(max(opt.topopt.x(opt.topopt.indActive),1e-3).*opt.topopt.Hs);
    elseif strcmp(opt.Filt,'dens') && ~strcmp(opt.Projection,'yes')
        dc(opt.topopt.indActive) = opt.topopt.H*(dc(opt.topopt.indActive)./opt.topopt.Hs);
    elseif strcmp(opt.Filt,'dens') && strcmp(opt.Projection,'yes')
        dc(opt.topopt.indActive) = opt.topopt.H*(dc(opt.topopt.indActive).*dx(opt.topopt.indActive)./opt.topopt.Hs);
    end
    
    % Store objective function for plotting
    opt.topopt.objective(itr)=abs(c);
    opt.topopt.xmat(:,itr) = opt.topopt.xPhys;
    % Scaling of objective function and sensitivities
    if itr == 1
        opt.topopt.cScale = 10/c;
        %opt.topopt.cScale = 1;
    end
    c = c*opt.topopt.cScale;
    %c0 = c; % for FD check
    dc = dc*opt.topopt.cScale;
    %dc0 = dc;
    
    %c = 1/c/log(10)*dc;
    %c = log10(c);
    c0 = c;
    dc0= dc;
    %c = c^2;
    %dc = 2*c*dc;
    
    %% Constraint functions and derivatives
    g = sum(opt.topopt.xPhys(opt.topopt.indActive))./(opt.topopt.volfrac*numel(opt.topopt.indActive))-1;
    dg = ones(1,numel(opt.topopt.indActive))./(opt.topopt.volfrac*numel(opt.topopt.indActive));
    if strcmp(opt.Filt,'dens') && ~strcmp(opt.Projection,'yes')
        dg = (opt.topopt.H*(dg'./opt.topopt.Hs))';
    end
    if strcmp(opt.Filt,'dens') && strcmp(opt.Projection,'yes')
       dg = (opt.topopt.H*(dg'.*dx(opt.topopt.indActive)./opt.topopt.Hs))';
    end
    
    opt.topopt.vol(itr) = g; % store vol constraint
    % Printing
    
    
    %% FDcheck
    if strcmp(opt.topopt.FDcheck,'yes') %&& itr==1
        FDcheck(mesh,study,opt,opt.topopt.x,c0,dc0,g,dg,intpmethod,FDelement)
    end
    %% Optimizer
    if strcmp(opt.topopt.optmzer,'MMA')
        [xnew,opt] = MMA(opt,mmaItr,c,dc(opt.topopt.indActive),g,dg);
    elseif strcmp(opt.topopt.optmzer,'OC')
        [xnew] = OC(opt,opt.topopt.x(opt.topopt.indActive),dc(opt.topopt.indActive),g,dg');
    end
    opt.topopt.x(opt.topopt.indActive) = xnew;
    change = max(abs(opt.topopt.x(opt.topopt.indActive)-opt.topopt.xold(opt.topopt.indActive)));
    
    disp([' It.: ' sprintf('%4i',itr) ' Obj.: ' sprintf('%10.4f',c) ...
        ' Vol.: ' sprintf('%6.3f',g) ...
        ' ch.: ' sprintf('%6.3f',change )])
    %% Plotting
    h3=figure(3);
    xplot = reshape(opt.topopt.xPhys,opt.nely,opt.nelx);
    colormap(gray);
    handle = subplot(2,1,1);
    imagesc(-[real(xplot)]); axis equal; axis tight;set(gca,'YDir','normal');pause(1e-30);colorbar;
    subplot(2,1,2)
    plot(opt.topopt.objective)
    xlabel('iteration','Fontsize',16)
    ylabel('\phi','Fontsize',16)
    title(projectname)
    saveas(h3,'Objective.png');
    %% Para view writing for every nth iteration
    if mod(itr,nthplot)==1 && strcmp(itPlot,'yes')
        opt.topopt.dc = dc;
        mkdir(['PLOT/' projectname])
        save(['PLOT/' projectname '/' 'opt.mat'],'opt')
        %save(['PLOT/' projectname '/' 'objective_' num2str(itr) '.mat'],'opt.topopt.objective')
        %save(['PLOT/' projectname '/' 'vol_' num2str(itr) '.mat'],'opt.topopt.vol')
        %ParaWrite(opt,study,mesh,projectname)
    end
    
end





