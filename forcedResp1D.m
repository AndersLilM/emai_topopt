
function [u] = forcedResp1D(omega,x_eq,V)
%omega = min(omega):0.001:max(omega);
k = 100;d0=1;eps = 1;
m = 1;
%k = 100;eps = 1; d0 = 1;
 u1 = 0:0.0001:0.34;
 phi1 = sqrt(k*u1.*(d0-u1).^2*2*1/eps);
% u2 = 0.33:0.0001:1;
% phi2 = sqrt(k*u2.*(d0-u2).^2*2*1/eps);

%VPi = sqrt((8*k*d0^3/(27*eps)));

%omega0 = sqrt(k/m);
%omega = sqrt(k/m-eps.*phi1.^2./(m*(d0-u1).^3));

%omega = omega/omega0;

x_eq = interp1(phi1,u1,V);

%(1/2)*phi^2*varepsilon/(k*d^2)

% plot(phi1/VPi,u1/d0,'k')
% hold on
% plot(phi2/VPi,u2/d0,'k--')
% set(gca,'Fontsize',17)
% axis([0 1.1 0 1])
% 
% xlabel('$\frac{V}{V_{PI}}$','interpreter','Latex','Fontsize',25)
% ylabel('$\frac{x}{d_0}$','interpreter','Latex','Fontsize',25)
% sti='/Users/Anders/Dropbox/DTU/11.Speciale/Thesis/Pictures/ElectrostaticTheory/';
% %pdfPrint('1DVeri',sti)
% 
% figure
% plot(phi1/VPi,omega,'k')
% set(gca,'Fontsize',17)
% axis([0 1.1 0 1.1])
% xlabel('$\frac{V}{V_{PI}}$','interpreter','Latex','Fontsize',25)
% ylabel('$\frac{\omega}{\omega_0}$','interpreter','Latex','Fontsize',25)
% sti='/Users/Anders/Dropbox/DTU/11.Speciale/Thesis/Sections/Theory/';
% %pdfPrint('1DVeriDyna',sti)
%figure

%x_eq = 0;
%V = sqrt(k*x_eq.*(d0-x_eq).^2*2*1/eps);
%V = 5;
f = 1;

%omega = 0:0.01:13;
%u=(((eps*V^2.*(m.*omega.^2+f-k).^2).^(1/3)./(m*omega.^2+f-k)+d0));
%u =-f*(d0-x_eq)^3./(d0^3*m*omega.^2-3*d0^2*m*omega.^2*x_eq+3*d0*m*omega.^2*x_eq^2-m*omega.^2*x_eq^3-d0^3*k+3*d0^2*k*x_eq-3*d0*k*x_eq^2+k*x_eq^3+V^2*eps);
u=abs(f./(-m*omega.^2+k-eps*V^2./(d0-x_eq)^3));
%semilogy(omega/omega0,abs(u)/d0,'-k','linewidth',1.2)
%hold on
%txt1 = ['$\leftarrow ' num2str(round(V/VPi,3)) '\, V_{PI}$'];
%[a index] = max(u);

%[umax index]= max(u);
%if umax == inf
%[ulist a] = sort(u);
%umax = ulist(end-1);


%text(omega(index)/omega0,umax,txt1,'interpreter','Latex','Fontsize',14)
%set(gca,'Fontsize',17)
%axis([0 omega(end)/omega0 min(u) max(u)*1.6])
%xlabel('$\frac{\Omega}{\omega_0}$','interpreter','Latex','Fontsize',25)
%ylabel('$\frac{\hat{x}}{d_0}$','interpreter','Latex','Fontsize',25)


end
%pdfPrint('1DforcedResp',sti)